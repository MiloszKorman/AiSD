package model;

import service.Save;

import java.util.ArrayList;

public class AcademicClass {

    private ArrayList<Student> students = new ArrayList<>();
    private Save saver;

    public AcademicClass(Save saver) {
        this.saver = saver;
    }

    public void addStudent(Student studentToAdd) {

        if (studentToAdd != null) {
            students.add(studentToAdd);
        }

    }

    public void removeStudent(Student studentToRemove) {
        if (students.contains(studentToRemove)) {
            students.remove(studentToRemove);
        }
    }

    public Student findStudent(String name, String surname) {

        for(Student student : students) {
            if(student.getName().equals(name) && student.getSurname().equals(surname)) {
                return student;
            }
        }

        System.out.println("Unfortunately there is no such student in this class");

        return null;
    }

    public double calcAverageDegree() {

        double averageDegree = 0;

        if (!students.isEmpty()) {

            for (Student student : students) {
                averageDegree += student.getDegree();
            }

            averageDegree /= students.size();

        }

        return averageDegree;
    }

    public void printStudents() {

        for (Student student : students) {
            System.out.println(student);
        }

    }

    public void exportStudents() {
        saver.exportStudents(students);
    }

    public void importStudents() {
        students = saver.importStudents();
    }

    public ArrayList<Student> getStudents() {
        return students;
    }
}
