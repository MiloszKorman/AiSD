package model;

import java.util.ArrayList;

public class Student {

    private String name;
    private String surname;
    private int indeks;
    private int academicTerm;
    private int academicYear;
    private ArrayList<Integer> lists = new ArrayList<>();
    private int pointsSum = 0;
    private int maxPoints;
    private double degree = 2.0;

    public Student(String name, String surname, int maxPoints, int indeks, int academicTerm, int academicYear) {
        this.name = name;
        this.surname = surname;
        this.maxPoints = maxPoints;
        this.indeks = indeks;
        this.academicYear = academicYear;
        this.academicTerm = academicTerm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getIndeks() {
        return indeks;
    }

    public void setIndeks(int indeks) {
        this.indeks = indeks;
    }

    public int getAcademicTerm() {
        return academicTerm;
    }

    public void setAcademicTerm(int academicTerm) {
        this.academicTerm = academicTerm;
    }

    public int getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(int academicYear) {
        this.academicYear = academicYear;
    }

    public ArrayList<Integer> getLists() {
        return lists;
    }

    public int getListsNumber() {
        return lists.size();
    }

    public int getPointsEarnedInList(int listNumber) {

        if (listNumber <= lists.size()) {
            return lists.get(listNumber - 1);
        } else {
            return -1;
        }

    }

    public void setPointsEarnedInList(int listNumber, int pointsEarned) {

        if (0 < listNumber) {

            if (maxPoints < pointsEarned) {
                pointsEarned = maxPoints;
            }

            if (listNumber <= lists.size()) {
                int oldValue = lists.get(listNumber - 1);
                lists.set(listNumber - 1, pointsEarned);
                updatePointsSum(oldValue, pointsEarned);
            } else {

                while (lists.size() < listNumber - 1) {
                    lists.add(0);
                }

                lists.add(pointsEarned);
                updatePointsSum(0, pointsEarned);

            }

        }

    }

    public void addPointsToList(int listNumber, int pointsToAdd) {

        if (0 < listNumber && listNumber <= lists.size()) {

            if (maxPoints < lists.get(listNumber - 1) + pointsToAdd) {
                pointsToAdd = maxPoints - lists.get(listNumber - 1);
            }

            updatePointsSum(lists.get(listNumber - 1), lists.get(listNumber - 1) + pointsToAdd);
            lists.set(listNumber - 1, lists.get(listNumber - 1) + pointsToAdd);
        }

    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    private void updatePointsSum(int prevValue, int newValue) {
        pointsSum += newValue - prevValue;
        updateDegree();
    }

    private void updateDegree() {

        double pointsAverage = ((double) pointsSum / (maxPoints * (lists.size()))) * 100;

        if (pointsAverage < 0 || 100 < pointsAverage) {
            degree = -1;
        } else {

            if (pointsAverage <= 50) {
                degree = 2;
            } else if (pointsAverage <= 60) {
                degree = 3;
            } else if (pointsAverage <= 70) {
                degree = 3.5;
            } else if (pointsAverage <= 80) {
                degree = 4;
            } else if (pointsAverage <= 90) {
                degree = 4.5;
            } else {
                degree = 5;
            }

        }

    }

    public double getDegree() {
        return degree;
    }

    @Override
    public String toString() {
        return name + " | " + surname + " | " + indeks + " | "+ "Year: " + academicYear + " | " + "Term: " + academicTerm  + " | " + degree;
    }

}
