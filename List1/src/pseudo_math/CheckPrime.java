package pseudo_math;

public class CheckPrime {

    public static boolean isPrime(int number) {

        if (number <= 1) {
            return false;
        } else if (number <= 3) {
            return true;
        } else if (number % 2 == 0 || number % 3 == 0) {
            return false;
        }

        //from 5 checks all odd numbers, smaller then root of number to check, according to wikipedia we
        //can check that way if a number has dividers other than 1 and itself, no need for more operations
        for (int i = 5; i * i <= number; i += 2) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

}
