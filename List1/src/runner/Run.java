package runner;

import model.AcademicClass;
import model.Student;
import service.SaveCsv;

import java.util.Scanner;

public class Run {

    private static Scanner sc = new Scanner(System.in);
    private static AcademicClass myClass = new AcademicClass(new SaveCsv());

    public static void main(String[] args) {
        start();
    }

    private static void start() {

        int choice = 0;

        while(choice != 8) {

            System.out.println("What do you want to do?:");
            System.out.println();
            System.out.println("1.Add new student");
            System.out.println("2.Change students info");
            System.out.println("3.Remove student");
            System.out.println("4.Print all students");
            System.out.println("5.Calculate average Degree");
            System.out.println("6.Import from csv");
            System.out.println("7.Export to csv");
            System.out.println("8.End");

            choice = getInt(1, 8);

            switch (choice) {
                case 1:
                    myClass.addStudent(studentCreation());
                    break;
                case 2:
                    changeStudentInfo();
                    break;
                case 3:
                    String[] nameNSurname = getNameNSurname();
                    Student studentToRemove = myClass.findStudent(nameNSurname[0], nameNSurname[1]);
                    if(studentToRemove != null) {
                        myClass.removeStudent(studentToRemove);
                    }
                    break;
                case 4:
                    myClass.printStudents();
                    break;
                case 5:
                    System.out.println("Average degree: " + myClass.calcAverageDegree());
                    break;
                case 6:
                    myClass.importStudents();
                    break;
                case 7:
                    myClass.exportStudents();
                    break;
            }


        }

        sc.close();

    }

    private static Student studentCreation() {
        String nameNSurname[] = getNameNSurname();
        System.out.println("Provide max points for all lists.");
        int maxPoints = getInt(1, 100);
        System.out.println("Provide students indeks.");
        int indeks = getInt(100000, 999999);
        System.out.println("Provide on which year is this student.");
        int year = getInt(1, 5);
        System.out.println("Provide on which term is this student.");
        int term = getAcademicTerm(year);

        Student student = new Student(
                nameNSurname[0],
                nameNSurname[1],
                maxPoints,
                indeks,
                term,
                year
        );

        System.out.println("Provide how many lists student finished.");
        int howManyLists = getInt(1, 15);

        for(int i = 1; i <= howManyLists; i++) {
            System.out.println("How many points student got on list " + i + "?");
            student.setPointsEarnedInList(i, getInt(1, maxPoints));
        }

        return student;
    }

    private static void changeStudentInfo() {

        String[] nameNSurname = getNameNSurname();
        Student studentToEdit = myClass.findStudent(nameNSurname[0], nameNSurname[1]);

        if(studentToEdit != null) {

            System.out.println("What do you want to change?:");
            System.out.println("1.Name");
            System.out.println("2.Surname");
            System.out.println("3.Indeks");
            System.out.println("4.Academic year and term");
            System.out.println("5.Academic term");
            System.out.println("6.Add points for new list");
            System.out.println("7.Change number of points in one of previous lists");
            System.out.println("8.Add points to one of previous lists");
            System.out.println("9.Go back");

            int choice = 0;

            while (choice != 9) {

                choice = getInt(1, 9);

                switch (choice) {
                    case 1:
                        System.out.print("Provide name: ");
                        studentToEdit.setName(sc.nextLine().split("\\s")[0]);
                        break;
                    case 2:
                        System.out.print("Provide surname: ");
                        studentToEdit.setSurname(sc.nextLine().split("\\s")[0]);
                        break;
                    case 3:
                        System.out.println("Provide indeks.");
                        studentToEdit.setIndeks(getInt(100000, 999999));
                        break;
                    case 4:
                        System.out.println("Provide academic year.");
                        studentToEdit.setAcademicYear(getInt(1, 5));
                    case 5:
                        System.out.println("Provide academic term.");
                        studentToEdit.setAcademicTerm(getAcademicTerm(studentToEdit.getAcademicYear()));
                        break;
                    case 6:
                        System.out.println("How many points student got?");
                        studentToEdit.setPointsEarnedInList(
                                studentToEdit.getListsNumber() + 1,
                                getInt(1, studentToEdit.getMaxPoints())
                        );
                        break;
                    case 7:
                        System.out.println("Which list do you want to edit?");
                        int list = getInt(1, studentToEdit.getListsNumber());
                        System.out.println("How many points do you want to set?");
                        int points = getInt(1, studentToEdit.getMaxPoints());
                        studentToEdit.setPointsEarnedInList(list, points);
                        break;
                    case 8:
                        System.out.println("To which list do you want to add points?");
                        list = getInt(1, studentToEdit.getListsNumber());
                        System.out.println("How many points do you want to add?");
                        points = getInt(0, studentToEdit.getMaxPoints() - studentToEdit.getPointsEarnedInList(list));
                        studentToEdit.addPointsToList(list, points);
                        break;
                }

            }

        }


    }

    private static int getInt(int smallest, int biggest) {

        int result = -1;

        while (result == -1) {

            System.out.print("Provide an integer between " + smallest + " and " + biggest + ": ");

            try {
                result = Integer.parseInt(sc.nextLine().split("\\s")[0]);

                if(result < smallest || biggest < result) {
                    result = -1;
                }

            } catch (NumberFormatException nfe) {
                result = -1;
            }

        }

        return result;
    }

    private static String[] getNameNSurname() {

        System.out.print("Provide students name and surname: ");

        String[] nameNSurname = {};

        while (nameNSurname.length != 2) {

            nameNSurname = sc.nextLine().split("\\s+");

            if(nameNSurname.length != 2) {
                System.out.println("It should be two separate elements");
            }

        }

        return nameNSurname;
    }

    private static int getAcademicTerm(int academicYear) {

        int term = 0;

        switch (academicYear) {
            case 1:
                term = getInt(1, 2);
                break;
            case 2:
                term = getInt(3, 4);
                break;
            case 3:
                term = getInt(5, 6);
                break;
            case 4:
                term = getInt(7, 8);
                break;
            case 5:
                term = getInt(9, 10);
                break;
        }

        return term;
    }

}
