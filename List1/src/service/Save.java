package service;

import model.Student;

import java.util.ArrayList;
import java.util.Collection;

public interface Save {

    void exportStudents(Collection<Student> students);

    ArrayList<Student> importStudents();

}
