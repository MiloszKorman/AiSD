package service;

import model.Student;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

public class SaveCsv implements Save {

    @Override
    public void exportStudents(Collection<Student> students) {

        try {

            BufferedWriter writer = new BufferedWriter(new FileWriter("students.csv"));

            for (Student student : students) {

                writer.append(student.getName() + ",")
                        .append(student.getSurname() + ",")
                        .append(student.getIndeks() + ",")
                        .append(student.getAcademicYear() + ",")
                        .append(student.getAcademicTerm() + ",")
                        .append(student.getMaxPoints() + "");

                student.getLists().forEach(pointsInList -> {

                    try {
                        writer.append("," + pointsInList);
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                });

                writer.newLine();
                writer.flush();

            }

            writer.close();

        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    @Override
    public ArrayList<Student> importStudents() {

        try {

            BufferedReader reader = new BufferedReader(new FileReader("students.csv"));

            String studentLine;

            ArrayList<Student> students = new ArrayList<>();

            while ((studentLine = reader.readLine()) != null) {

                String[] studentInfo = studentLine.split(",");

                students.add(new Student(
                        studentInfo[0],
                        studentInfo[1],
                        Integer.parseInt(studentInfo[5]),
                        Integer.parseInt(studentInfo[2]),
                        Integer.parseInt(studentInfo[4]),
                        Integer.parseInt(studentInfo[3])
                ));

                for (int i = 6; i < studentInfo.length; i++) {
                    students.get(students.size() - 1).setPointsEarnedInList(i - 5, Integer.parseInt(studentInfo[i]));
                }

            }

            reader.close();

            return students;

        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return null;
    }

}
