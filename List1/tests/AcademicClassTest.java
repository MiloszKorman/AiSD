import model.AcademicClass;
import model.Student;
import org.junit.Before;
import org.junit.Test;
import service.SaveCsv;

import static org.junit.Assert.*;

public class AcademicClassTest {

    private AcademicClass academicClass;

    @Before
    public void setUp() {
        academicClass = new AcademicClass(new SaveCsv());
    }

    @Test
    public void addStudentTest() {

        academicClass.addStudent(new Student("Jurek", "Kirek", 5, 242430, 2, 1));
        assertEquals(1, academicClass.getStudents().size());

        academicClass.addStudent(null);
        assertEquals(1, academicClass.getStudents().size());

    }

    @Test
    public void removeStudentTest() {

        Student student = new Student("Jurek", "Kirek", 5, 242430, 2, 1);

        academicClass.addStudent(student);
        assertEquals(1, academicClass.getStudents().size());

        academicClass.removeStudent(student);
        assertTrue(academicClass.getStudents().isEmpty());

    }

    @Test
    public void csvImportAndExportTest() {

        academicClass.addStudent(new Student("Jurek", "Kirek", 5, 242430, 2, 1));
        academicClass.addStudent(new Student("Mirek", "Kirek", 4, 212578, 3, 2));

        academicClass.exportStudents();
        academicClass.importStudents();

        assertEquals(2, academicClass.getStudents().size());

    }

    @Test
    public void averageDegreeTest() {

        assertEquals(0, academicClass.calcAverageDegree(), 0.1);

        Student student1 = new Student("...", "...", 5, 242430, 2, 1);
        student1.setPointsEarnedInList(1, 5);
        Student student2 = new Student("...", "...", 4, 242430, 1, 1);
        student2.setPointsEarnedInList(1, 2);
        Student student3 = new Student("...", "...", 6, 242430, 3, 2);
        student3.setPointsEarnedInList(1, 4);
        Student student4 = new Student("...", "...", 3, 242430, 5, 3);
        student4.setPointsEarnedInList(1, 3);

        academicClass.addStudent(student1);
        academicClass.addStudent(student2);
        academicClass.addStudent(student3);
        academicClass.addStudent(student4);

        assertEquals(3.8, academicClass.calcAverageDegree(), 0.1);

        academicClass.removeStudent(student2);
        academicClass.removeStudent(student3);

        assertEquals(5.0, academicClass.calcAverageDegree(), 0.1);

    }

}