import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static pseudo_math.CheckPrime.isPrime;

public class CheckPrimeTest {

    @Test
    public void isPrimeTest() {

        assertTrue(isPrime(2));
        assertTrue(isPrime(7));
        assertTrue(isPrime(47));
        assertFalse(isPrime(49));
        assertFalse(isPrime(0));
        assertFalse(isPrime(-7));

    }

}