import model.Student;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.*;

public class StudentTest {

    private Student student;

    @Before
    public void setUp() {
        student = new Student("Jurek", "Ogurek", 5, 242430, 2, 1);
    }

    @Test
    public void setPointsEarnedInListTest() {

        student.setPointsEarnedInList(1, 3);
        student.setPointsEarnedInList(2, 8);
        assertEquals(2, student.getListsNumber());
        assertEquals(3, student.getPointsEarnedInList(1));
        assertEquals(5, student.getPointsEarnedInList(2));

        student.setPointsEarnedInList(5, 10);
        assertEquals(5, student.getListsNumber());

        student.setPointsEarnedInList(-5, 3);
        assertEquals(5, student.getListsNumber());

    }

    @Test
    public void addPointsToListTest() {

        student.setPointsEarnedInList(1, 3);
        student.addPointsToList(1, 2);
        assertEquals(5, student.getPointsEarnedInList(1));

        int size = student.getListsNumber();
        student.addPointsToList(15, 2);
        assertEquals(size, student.getListsNumber());

    }

    @Test
    public void degreeCalculationTest() {

        student.setPointsEarnedInList(1, 2);
        student.setPointsEarnedInList(2, 2);
        assertEquals(2, student.getDegree(), 0.1);

        student.setPointsEarnedInList(1, 3);
        student.setPointsEarnedInList(2, 3);
        assertEquals(3, student.getDegree(), 0.1);

        student.setPointsEarnedInList(1, 4);
        student.setPointsEarnedInList(2, 3);
        assertEquals(3.5, student.getDegree(), 0.1);

        student.setPointsEarnedInList(1, 4);
        student.setPointsEarnedInList(2, 4);
        assertEquals(4, student.getDegree(), 0.1);

        student.setPointsEarnedInList(1, 5);
        student.setPointsEarnedInList(2, 4);
        assertEquals(4.5, student.getDegree(), 0.1);

        student.setPointsEarnedInList(1, 5);
        student.setPointsEarnedInList(2, 5);
        assertEquals(5, student.getDegree(), 0.1);

    }

    @Test
    public void degreeCalculationWithAddTest() {

        student.setPointsEarnedInList(1, 2);
        student.setPointsEarnedInList(2, 2);
        assertEquals(2, student.getDegree(), 0.1);

        student.addPointsToList(1, 1);
        student.addPointsToList(2, 1);
        assertEquals(3, student.getDegree(), 0.1);

        student.addPointsToList(1, 1);
        assertEquals(3.5, student.getDegree(), 0.1);

        student.addPointsToList(2, 1);
        assertEquals(4, student.getDegree(), 0.1);

        student.addPointsToList(1, 1);
        assertEquals(4.5, student.getDegree(), 0.1);

        student.addPointsToList(2, 1);
        assertEquals(5, student.getDegree(), 0.1);

    }

}