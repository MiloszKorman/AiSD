import graph.Graph;
import graph.MatrixGraph;
import graph.services.GraphVisualizationService;
import graph.services.SwingGraphVisualizationService;

public class Runner {

    public static void main(String[] args) {

        Graph graph = new MatrixGraph(6);

        graph.addEdge(0, 4, 3);
        graph.addEdge(0, 5, 6);
        graph.addEdge(4, 5, 2);
        graph.addEdge(0, 1, 3);
        graph.addEdge(1, 2, 1);
        graph.addEdge(2, 5, 1);
        graph.addEdge(1, 3, 3);
        graph.addEdge(2, 3, 3);
        graph.addEdge(5, 3, 1);

        GraphVisualizationService visualizationService = new SwingGraphVisualizationService();

        visualizationService.visualizeWithShortestPath(
                graph,
                ShortestPathBellmanFordFinder.findShortestPath(graph, 0, 3)
        );

    }

}
