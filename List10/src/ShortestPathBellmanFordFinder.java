import graph.Graph;
import graph.Graph.Link;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ShortestPathBellmanFordFinder {

    public static List<Graph.Link> findShortestPath(Graph graph, int start, int end) {

        if (start < 0 || graph.size() <= start) {
            throw new IllegalArgumentException("number provided not in range of accessible numbers");
        }
        if (end < 0 || graph.size() <= end) {
            throw new IllegalArgumentException("number provided not in range of accessible numbers");
        }

        List<Integer> d = new ArrayList<>(graph.size());
        List<Integer> p = new ArrayList<>(graph.size());

        for (int i = 0; i < graph.size(); i++) {
            d.add(Integer.MAX_VALUE);
            p.add(-1);
        }

        d.set(start, 0);

        for (int i = 2; i <= graph.size(); i++) {
            for (int x = 0; x < graph.size(); x++) {
                int currentX = x;
                List<Link> eXesLinks = graph.getAllLinks().stream()
                        .filter(link -> link.firstNode == currentX || link.secondNode == currentX)
                        .collect(Collectors.toList());
                for (Link link : eXesLinks) {
                    if (link.firstNode == x) {
                        if (d.get(link.secondNode) <= d.get(link.firstNode) + link.weight) {
                            continue;
                        }
                        d.set(link.secondNode, d.get(link.firstNode) + link.weight);
                        p.set(link.secondNode, link.firstNode);
                    } else {
                        if (d.get(link.firstNode) <= d.get(link.secondNode) + link.weight) {
                            continue;
                        }
                        d.set(link.firstNode, d.get(link.secondNode) + link.weight);
                        p.set(link.firstNode, link.secondNode);
                     }
                }
            }
        }

        List<Link> path = new ArrayList<>();
        int current = end;

        while (current != start && current != -1) {
            for (Link link : graph.getAllLinks()) {
                if ((link.firstNode == current && link.secondNode == p.get(link.firstNode)) ||
                        (link.secondNode == current && link.firstNode == p.get(link.secondNode))) {

                    path.add(link);
                }
            }
            current = p.get(current);
        }

        for (int x = 0; x < graph.size(); x++) {
            int currentX = x;
            List<Link> eXesLinks = graph.getAllLinks().stream()
                    .filter(link -> link.firstNode == currentX || link.secondNode == currentX)
                    .collect(Collectors.toList());

            for (Link link : eXesLinks) {
                if (x == link.firstNode) {
                    if (d.get(link.secondNode) > d.get(link.firstNode) + link.weight) {
                        path.clear();
                    }
                } else {
                    if (d.get(link.firstNode) > d.get(link.secondNode) + link.weight) {
                        path.clear();
                    }
                }
            }
        }

        if (current == -1) { //if we overflow to -1, then there's no path from end to start
            path.clear();
        }

        Collections.sort(path);

        if (end < start) {
            Collections.reverse(path);
            path.forEach(link -> {
                int holder = link.secondNode;
                link.secondNode = link.firstNode;
                link.firstNode = holder;
            });
        }

        return path;
    }

}
