import graph.Graph;
import graph.Graph.Link;
import graph.MatrixGraph;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ShortestPathBellmanFordFinderTest {

    @Test
    public void testSameResultAsDijkistra() {

        //given
        int start = 0;
        int end = 3;

        Graph graph = new MatrixGraph(6);

        graph.addEdge(0, 4, 3);
        graph.addEdge(0, 5, 6);
        graph.addEdge(4, 5, 2);
        graph.addEdge(0, 1, 3);
        graph.addEdge(1, 2, 1);
        graph.addEdge(2, 5, 1);
        graph.addEdge(1, 3, 3);
        graph.addEdge(2, 3, 3);
        graph.addEdge(5, 3, 1);

        //when
        List<Link> dijkistraResults = ShortestPathBellmanFordFinder.findShortestPath(graph, start, end);

        List<Link> bellmanFordResults = ShortestPathBellmanFordFinder.findShortestPath(graph, start, end);


        //then
        assertEquals(
                dijkistraResults.stream()
                        .map(link -> link.weight)
                        .reduce(0, (acc, weight) -> acc + weight),
                bellmanFordResults.stream()
                        .map(link -> link.weight)
                        .reduce(0, (acc, weight) -> acc + weight)
        );

    }

    @Test
    public void testNoPath() {

        //given
        int start = 0;
        int end = 2;
        Graph graph = new MatrixGraph(4);
        graph.addEdge(0,1);
        graph.addEdge(1, 3);

        //when
        List<Link> results = ShortestPathBellmanFordFinder.findShortestPath(graph, start, end);

        //then
        assertTrue(results.isEmpty());

    }

}