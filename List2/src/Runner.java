import data_structures.ArrayList;
import data_structures.LinkedList;
import model.Student;
import service.SaveCsv;

public class Runner {

    public static void main(String[] args) {

        SaveCsv csv = new SaveCsv();

        System.out.println("My ArrayList:");
        ArrayList<Student> arrayListOfStudents = new ArrayList<>(csv.importStudents());
        arrayListOfStudents.forEach(System.out::println);

        System.out.println();

        System.out.println("My LinkedList:");
        LinkedList<Student> linkedListOfStudents = new LinkedList<>(csv.importStudents());
        linkedListOfStudents.forEach(System.out::println);


    }

}
