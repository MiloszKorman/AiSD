package data_structures;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E>, Iterable<E> {

    private Object[] elements;
    private int size = 0;

    private final double UPPER_THRESHOLD = 0.9;
    private final double LOWER_THRESHOLD = 0.7;

    public ArrayList() {
        this(10);
    }


    public ArrayList(int initialSize) {
        if (0 <= initialSize) {
            elements = new Object[initialSize];
        }
    }

    public ArrayList(Collection<E> collection) {
        this();
        addAll(collection);
    }

    @Override
    public int size() {
        return size;
    }

    //Goes from beginning of array until meets the element, then returns at which iteration it happened
    @Override
    public int indexOf(Object object) {
        if (object == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (object.equals(elements[i])) {
                    return i;
                }
            }
        }

        return -1;
    }

    //swaps element at this index for new one
    @Override
    public void set(int index, E element) {
        checkIndex(index);

            elements[index] = element;

    }

    //checks whether array isn't to small, adds the element at the end
    @Override
    public void add(E element) {
        if (isToSmall(size + 1)) {
            enlarge(size + 1);
        }
        elements[size++] = element;
    }

    //checks whether array isn't to small, adds the element at correct index, shifts the rest of array to the right
    @Override
    public void add(int index, E element) {
        checkIndexAdd(index);

            if (isToSmall(size + 1)) {
                enlarge(size + 1);
            }

            arraycopy(elements, index, index + 1, size - index);
            elements[index] = element;
            size++;

    }

    //copies array without element on said index, checks whether new array is not too big
    @Override
    public void remove(int index) throws IndexOutOfBoundsException {
        checkIndex(index);

            //remove dude
            arraycopy(elements, index + 1, index, size - index - 1);
            size--;

            if (isToBig(size)) {
                decrease(size);
            }


    }

    //sets up new empty array with default length
    @Override
    public void clear() {
        elements = new Object[10];
        size = 0;
    }

    //adds all elements from collection at the end, returns true if collection wasn't empty
    @Override
    public boolean addAll(Collection<? extends E> collection) {
        return addAll(size, collection);
    }

    //makes new array, inserts collection in specified index, returns true if collection wasn't empty
    @Override
    public boolean addAll(int index, Collection<? extends E> collection) {
        checkIndexAdd(index);

        if(isToSmall(size + collection.size())) {
            enlarge(size + collection.size());
        }

        if(collection == null) {
            throw new NullPointerException();
        }

        Object[] collectionToAdd = collection.toArray();
        arraycopy(collectionToAdd,0,index,collectionToAdd.length);
        size += collectionToAdd.length;
        return collectionToAdd.length != 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E get(int index) throws IndexOutOfBoundsException {
        checkIndex(index);

            return (E) elements[index];

    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    //checks whether the array is above upper threshold
    private boolean isToSmall(int necessarySpace) {
        return (UPPER_THRESHOLD <= (necessarySpace / elements.length));
    }

    //checks whether the array is at least minimal size, if so checks whether it's below lower threshold
    private boolean isToBig(int necessarySpace) {
        if (10 < elements.length) {
            return (necessarySpace / elements.length) <= LOWER_THRESHOLD;
        } else {
            return false;
        }
    }

    //when array is too small, calculates what should be the new size of an array which cannot be smaller than
    //value provided, then copies our array to new, bigger one
    private void enlarge(int atLeastHowMuch) {
        int newLength = (int) ((elements.length * UPPER_THRESHOLD) / LOWER_THRESHOLD) - 1;

        if (newLength < atLeastHowMuch) {
            newLength = atLeastHowMuch;
        }

        elements = Arrays.copyOf(elements, newLength);
    }

    //when array is too big, calculates what should be the new size of an array which cannot be smaller than
    //value provided, then copies our array to new, smaller one
    private void decrease(int notSmallerThan) {
        int newLength = (int) ((elements.length * LOWER_THRESHOLD) / UPPER_THRESHOLD) + 1;

        if (notSmallerThan < 10) {
            notSmallerThan = 10;
        }

        if (newLength < notSmallerThan) {
            newLength = notSmallerThan;
        }

        elements = Arrays.copyOf(elements, newLength);
    }

    private class ArrayListIterator implements Iterator<E> {

        private int position = 0;

        @Override
        public boolean hasNext() {
            return position < size;
        }

        @Override
        @SuppressWarnings("unchecked")
        public E next() throws NoSuchElementException {
            if(hasNext()) {
                return (E) elements[position++];
            } else {
                throw new NoSuchElementException();
            }
        }
    }

    //Basically my copy of System.arraycopy :)
    private void arraycopy(Object src, int srcPos, int destPos, int length) {

        Object[] toCopy = (Object[]) src;

        Object[] newArray = new Object[elements.length + toCopy.length];

        //Copy all elements before where insertion happens
        for(int i = 0; i < destPos; i++) {
            newArray[i] = elements[i];
        }

        //insert all new elements
        int k = 0;
        for (int i = srcPos; i < srcPos + length; i++) {
            newArray[destPos + k] = toCopy[i];
            k++;
        }

        //copy the rest of original array
        k = 0;
        for(int i = destPos; i < elements.length; i++) {
            newArray[destPos + length + k] = elements[i];
            k++;
        }

        elements = newArray;
    }

    private void checkIndex(int index) {
        if (index < 0 || size <= index) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void checkIndexAdd(int index) {
        if (index < 0 || size < index) {
            throw new IndexOutOfBoundsException();
        }
    }

}
