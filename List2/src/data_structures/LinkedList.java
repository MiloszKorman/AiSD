package data_structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<E> implements List<E>, Iterable<E> {

    private int size = 0;

    private Node<E> first;
    private Node<E> last;

    public LinkedList() {
        first = new Node<>(null, null, null);
        last = new Node<>(null, null, null);
        first.next = last;
        last.previous = first;
    }

    public LinkedList(Collection<E> collection) {
        this();
        addAll(collection);
    }

    @Override
    public int size() {
        return size;
    }

    //starts from first, and goes with each iteration through next node, when node stores said value, number of
    //iterations performed is returned
    //if elements isn't found, then -1 is returned
    @Override
    public int indexOf(E element) {
        Node<E> currentElement = first;

        if (element == null) {

            for (int i = 1; i <= size - 1; i++) {
                if (currentElement.value == null) {
                    return i - 1;
                }
                currentElement = currentElement.next;
            }

        } else {

            for (int i = 1; i <= size - 1; i++) {
                if (currentElement.value.equals(element)) {
                    return i - 1;
                }
                currentElement = currentElement.next;
            }

        }

        return -1;
    }

    //if index is right after last elements, then element is set as new last node, otherwise, node on such index is
    //found and it's value is changed
    @Override
    public void set(int index, E element) {
        checkIndex(index);

        if (index == size) {
            Node<E> endNode = new Node<>(last, element, null);
            last.next = endNode;
            last = endNode;
        } else {
            findNode(index).value = element;
        }
    }

    @Override
    public void add(E element) {
        if (size == 0) {
            first.value = element;
        } else if (size == 1) {
            last.value = element;
        } else {
            Node<E> nodeToAdd = new Node<>(last, element, null);
            last.next = nodeToAdd;
            last = nodeToAdd;
        }

        size++;
    }

    //element is inserted on index and linked with neighbours
    @Override
    public void add(int index, E element) {

        checkIndexAdd(index);

        if (size == 0) {
            first.value = element;
        } else if (size == 1) {
            last.value = element;
        } else if (index == size) {
            Node<E> nodeToAdd = new Node<>(last, element, null);
            last.next = nodeToAdd;
            last = nodeToAdd;
        } else {
            Node<E> currentNode = findNode(index);

            Node<E> nodeToAdd = new Node<>(currentNode.previous, element, currentNode);
            currentNode.previous.next = nodeToAdd;
            currentNode.previous = nodeToAdd;
        }

        size++;
    }

    //we relink nodes on provided index neighbours next/previous values, so that this one is disconnected
    @Override
    public void remove(int index) throws IndexOutOfBoundsException {
        checkIndex(index);

        if (index == 0) {
            if (size <= 2) {
                first.value = last.value;
                last.value = null;
            } else {
                first.next.previous = null;
                first = first.next;
            }
        } else if (index == size - 1) {
            if (size == 2) {
                last.value = null;
            } else {
                last.previous.next = null;
                last = last.previous;
            }
        } else {
            Node<E> currentNode = findNode(index);
            currentNode.previous.next = currentNode.next;
            currentNode.next.previous = currentNode.previous;
        }

        size--;
    }

    @Override
    public void clear() {
        first.value = null;
        first.next = last;
        last.value = null;
        last.previous = first;
        size = 0;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        return addAll(size, collection);
    }

    //for each element in collection add with index method is used to add said element and shift the rest
    @Override
    @SuppressWarnings("unchecked")
    public boolean addAll(int index, Collection<? extends E> collection) {
        checkIndexAdd(index);

        if(collection == null) {
            throw new NullPointerException();
        }

        E[] toAdd = (E[]) collection.toArray();

        for (int i = 0; i < toAdd.length; i++) {
            add(index + i, toAdd[i]);
        }

        return (toAdd.length != 0);
    }

    @Override
    public E get(int index) throws IndexOutOfBoundsException {
        if (index < 0 || size <= index) {
            throw new IndexOutOfBoundsException();
        }

        return findNode(index).value;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator();
    }

    //depending on position of node in linked list we either start from beginning or end, depending which way is shorter
    //we go through next nodes until we make index number of iterations, then found node is returned
    private Node<E> findNode(int index) {

        checkIndex(index);

        Node<E> nodeToGet;

        if (index <= (size / 2 + 1)) {
            nodeToGet = first;
            for (int i = 1; i <= index; i++) {
                nodeToGet = nodeToGet.next;
            }
        } else {
            nodeToGet = last;
            for (int i = 1; i <= index; i++) {
                nodeToGet = nodeToGet.previous;
            }
        }

        return nodeToGet;
    }

    private static class Node<E> {
        Node<E> previous;
        Node<E> next;
        private E value;

        Node(Node<E> previous, E element, Node<E> next) {
            this.previous = previous;
            this.value = element;
            this.next = next;
        }
    }

    private class LinkedListIterator implements Iterator<E> {

        Node<E> currentNode = first;

        @Override
        public boolean hasNext() {
            return (currentNode != null);
        }

        @Override
        public E next() {
            if (hasNext()) {
                E elementToReturn = currentNode.value;
                currentNode = currentNode.next;
                return elementToReturn;
            } else {
                throw new NoSuchElementException();
            }
        }
    }

    private void checkIndex(int index) {
        if (index < 0 || size <= index) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void checkIndexAdd(int index) {
        if (index < 0 || size < index) {
            throw new IndexOutOfBoundsException();
        }
    }

}
