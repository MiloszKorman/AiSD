package data_structures;

import java.util.Collection;

public interface List<E> {

    int size();

    int indexOf(E element);

    //returns element which has been replaced, throws IndexOutOfBoundsException
    void set(int index, E element);

    //returns boolean
    void add(E element);

    void add(int index, E element);

    //returns E, throws IndexOutOfBoundsException
    void remove(int index);

    void clear();

    //<? extends E> returns boolean
    boolean addAll(Collection<? extends E> collection);

    //<? extends E> returns boolean
    boolean addAll(int index, Collection<? extends E> collection);

    //throws IndexOutOfBoundsException
    E get(int index);

}
