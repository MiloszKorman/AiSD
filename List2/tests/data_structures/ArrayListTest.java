package data_structures;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArrayListTest {

    ArrayList<Integer> list = new ArrayList<>();

    @Test
    void sizeTest() {
        ListTest.sizeTest(list);
    }

    @Test
    void indexOfTest() {
        ListTest.indexOfTest(list);
    }

    @Test
    void setTest() {
        ListTest.setTest(list);
    }

    @Test
    void addTest() {
        ListTest.addTest(list);
    }

    @Test
    void addByIndexTest() {
        ListTest.addByIndexTest(list);
    }

    @Test
    void removeTest() {
        ListTest.removeTest(list);
    }

    @Test
    void clearTest() {
        ListTest.clearTest(list);
    }

    @Test
    void addAllTest() {
        ListTest.addAllTest(list);
    }

    @Test
    void addAllFromIndexTest() {
        ListTest.addAllFromIndexTest(list);
    }

    @Test
    void getTest() {
        ListTest.getTest(list);
    }

    @Test
    void iteratorTest() {
        list.clear();

        for(int i = 1; i <= 15; i++) {
            list.add(i);
        }

        int i = 1;
        for(Integer element : list) {
            assertEquals(new Integer(i), element);
            i++;
        }

        assertEquals(16, i);
    }

}