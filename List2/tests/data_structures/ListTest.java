package data_structures;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ListTest {

    static void sizeTest(List<Integer> list) {

        //check whether size of an empty list is 0
        assertEquals(0, list.size());

        //check if size is properly calculated
        list.add(1);
        list.add(2);
        assertEquals(2, list.size());

        //check if it changes after removal of an element
        list.remove(1);
        assertEquals(1, list.size());
    }

    static void indexOfTest(List<Integer> list) {

        //check whether returns proper elements index
        list.add(1);
        list.add(2);
        list.add(3);
        assertEquals(1, list.indexOf(2));

        //check if method returns -1, when no such element has been added
        assertEquals(-1, list.indexOf(20));

        //check indexOf(null) should return first null it encounters
        list.clear();
        list.add(1);
        list.add(null);
        list.add(3);
        list.add(null);
        assertEquals(1, list.indexOf(null));
    }

    static void setTest(List<Integer> list) {

        //checks whether after element insertion, all elements are on right positions
        list.add(1);
        list.add(3);
        list.add(4);
        list.set(1, 2);
        assertEquals(new Integer(1), list.get(0));
        assertEquals(new Integer(2), list.get(1));
        assertEquals(new Integer(4), list.get(2));

        //index out of bounds exception should be thrown when we try to set element on nonexistent position
        assertThrows(IndexOutOfBoundsException.class, () -> list.set(-1, 13));
        assertThrows(IndexOutOfBoundsException.class, () -> list.set(10, 8));
    }

    static void addTest(List<Integer> list) {

        //checks whether element is indeed added to the list
        list.add(1);
        assertEquals(1, list.size());
        assertEquals(new Integer(1), list.get(0));

        //method should accept nulls
        list.clear();
        list.add(null);
        assertEquals(1, list.size());
    }

    static void addByIndexTest(List<Integer> list) {

        //method should accept nulls
        list.clear();
        list.add(0, null);
        assertEquals(1, list.size());

        //index out of bounds exception should be thrown when we try to add element on impossible positions
        // <0 || lastElementsIndex+1<
        assertThrows(IndexOutOfBoundsException.class, () -> list.add(-1, 5));
        assertThrows(IndexOutOfBoundsException.class, () -> list.add(4, 10));
    }

    static void removeTest(List<Integer> list) {

        //checks if after element removal, list size is decreased, and other elements stay intact
        list.add(1);
        list.add(2);
        list.add(3);
        list.remove(1);
        assertEquals(2, list.size());
        assertEquals(new Integer(1), list.get(0));
        assertEquals(new Integer(3), list.get(1));

        //TODO test out of bounds thrown
    }

    static void clearTest(List<Integer> list) {

        //check whether list is cleared correctly
        list.add(1);
        list.add(2);
        list.add(3);
        list.clear();
        assertEquals(0, list.size());
    }

    static void addAllTest(List<Integer> list) {

        list.clear();

        //check whether after adding a collection size is properly calculated
        HashSet<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        list.addAll(set);
        assertEquals(3, list.size());

        //check if trying to add empty collection causes false
        assertFalse(list.addAll(new HashSet<>()));

        //null pointer exception should be thrown when provided collection is null
        assertThrows(NullPointerException.class, () -> list.addAll(null));
    }

    static void addAllFromIndexTest(List<Integer> list) {

        //check whether after adding a collection in the middle of existing list, size is calculated properly
        list.add(1);
        list.add(2);
        list.add(3);
        HashSet<Integer> set = new HashSet<>();
        set.add(4);
        set.add(5);
        list.addAll(1, set);
        assertEquals(5, list.size());

        //check if trying to add empty collection causes false
        assertFalse(list.addAll(0, new HashSet<>()));

        //null pointer exception should be thrown when provided collection is null
        assertThrows(NullPointerException.class, () -> list.addAll(0, null));

        //index out of bounds exception should be thrown when we try to add element on impossible positions
        // <0 || size <=
        list.clear();
        assertThrows(IndexOutOfBoundsException.class, () -> list.addAll(-1, set));
        assertThrows(IndexOutOfBoundsException.class, () -> list.addAll(15, set));
    }

    static void getTest(List<Integer> list) {

        //check if proper object is returned
        list.add(1);
        list.add(2);
        list.add(3);
        assertEquals(new Integer(2), list.get(1));

        //index out of bounds exception should be thrown when we try to get element from nonexistent position
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(15));
    }

}