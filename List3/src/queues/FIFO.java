package queues;

import java.util.NoSuchElementException;

/**FIFO MyQueue*/
public class FIFO<E> extends MyQueue<E> {

    //throws exception when is empty, returns and removes first element otherwise
    @Override
    public E remove() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        } else {
            E toReturn = storage.get(0);
            storage.remove(0);
            return toReturn;
        }
    }

    //returns null when is empty, returns and removes first element otherwise
    @Override
    public E poll() {
        if(isEmpty()) {
            return null;
        } else {
            E toReturn = storage.get(0);
            storage.remove(0);
            return toReturn;
        }
    }

    //throws exception when is empty, returns first element otherwise
    @Override
    public E element() {
        if(isEmpty()) {
            throw new NoSuchElementException();
        } else {
            return storage.get(0);
        }
    }

    //returns null when is empty, returns first element otherwise
    @Override
    public E peek() {
        if(isEmpty()) {
            return null;
        } else {
            return storage.get(0);
        }
    }

}
