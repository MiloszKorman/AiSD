package queues;

import java.util.NoSuchElementException;

/**LIFO MyQueue*/
public class LIFO<E> extends MyQueue<E> {

    //throws exception when is empty, returns and removes last element otherwise
    @Override
    public E remove() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        } else {
            E toReturn = storage.get(storage.size() - 1);
            storage.remove(storage.size() - 1);
            return toReturn;
        }
    }

    //returns null when is empty, returns and removes last element otherwise
    @Override
    public E poll() {
        if(isEmpty()) {
            return null;
        } else {
            E toReturn = storage.get(storage.size() - 1);
            storage.remove(storage.size() - 1);
            return toReturn;
        }
    }

    //throws exception when is empty, returns last element otherwise
    @Override
    public E element() {
        if(isEmpty()) {
            throw new NoSuchElementException();
        } else {
            return storage.get(storage.size() - 1);
        }
    }

    //returns null when is empty, returns last element otherwise
    @Override
    public E peek() {
        if(isEmpty()) {
            return null;
        } else {
            return storage.get(storage.size() - 1);
        }
    }

}
