package queues;

import data_structures.LinkedList;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public abstract class MyQueue<E> implements Queue<E> {

    LinkedList<E> storage = new LinkedList<E>();

    @Override
    public int size() {
        return storage.size();
    }

    @Override
    public boolean isEmpty() {
        return storage.size() == 0;
    }

    @Override
    public Iterator<E> iterator() {
        return storage.iterator();
    }

    @Override
    public boolean add(E e) {
        storage.add(e);
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return storage.addAll(c);
    }

    @Override
    public void clear() {
        storage.clear();
    }

    @Override
    public boolean offer(E e) {
        storage.add(e);
        return true;
    }


    /*Unsupported methods*/
    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

}
