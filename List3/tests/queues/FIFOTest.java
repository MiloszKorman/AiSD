package queues;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class FIFOTest {

    FIFO<Integer> queue;

    @BeforeEach
    void setUp() {
        queue = new FIFO<>();
    }

    @Test
    void removeTest() {
        assertThrows(NoSuchElementException.class, queue::remove);
        queue.offer(1);
        queue.offer(2);
        assertEquals(new Integer(1), queue.remove());
        assertEquals(1, queue.size());
    }

    @Test
    void pollTest() {
        assertNull(queue.poll());
        queue.offer(1);
        queue.offer(2);
        assertEquals(new Integer(1), queue.poll());
        assertEquals(1, queue.size());
    }

    @Test
    void elementTest() {
        assertThrows(NoSuchElementException.class, queue::element);
        queue.offer(1);
        queue.offer(2);
        assertEquals(new Integer(1), queue.element());
        assertEquals(2, queue.size());
    }

    @Test
    void peekTest() {
        assertNull(queue.peek());
        queue.offer(1);
        queue.offer(2);
        assertEquals(new Integer(1), queue.peek());
        assertEquals(2, queue.size());
    }

    @Test
    void offerTest() {
        MyQueueTest.offerTest(queue);
    }


    /**Unsupported methods*/
    @Test
    void containsTest() {
        MyQueueTest.containsTest(queue);
    }

    @Test
    void toObjectArrayTest() {
        MyQueueTest.toObjectArrayTest(queue);
    }

    @Test
    void toGenericArrayTest() {
        MyQueueTest.toGenericArrayTest(queue);
    }

    @Test
    void removeObjectTest() {
        MyQueueTest.removeObjectTest(queue);
    }

    @Test
    void containsAllTest() {
        MyQueueTest.containsAllTest(queue);
    }

    @Test
    void removeAllTest() {
        MyQueueTest.removeAllTest(queue);
    }

    @Test
    void retainAllTest() {
        MyQueueTest.retainAllTest(queue);
    }


    /**Unnecessary tests, since LinkedList is used and has those same methods, we may assume that they work properly*/
    @Test
    void sizeTest() {
        MyQueueTest.sizeTest(queue);
    }

    @Test
    void isEmptyTest() {
        MyQueueTest.isEmptyTest(queue);
    }

    @Test
    void iteratorTest() {
        MyQueueTest.iteratorTest(queue);
    }

    @Test
    void addTest() {
        MyQueueTest.addTest(queue);
    }

    @Test
    void addAllTest() {
        MyQueueTest.addAllTest(queue);
        MyQueueTest.addAllSubclassTest(new FIFO<>());

        FIFO<FIFO> queueAddAllTest = new FIFO<>();
        ArrayList<FIFO> testList = new ArrayList<>();
        testList.add(new FIFO());
        testList.add(new FIFO());
        testList.add(new FIFO());
        queueAddAllTest.addAll(testList);
        assertEquals(3, queueAddAllTest.size());
    }

    @Test
    void clearTest() {
        MyQueueTest.clearTest(queue);
    }

}