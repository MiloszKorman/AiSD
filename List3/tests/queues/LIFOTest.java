package queues;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LIFOTest {

    LIFO<Integer> stack;

    @BeforeEach
    void setUp() {
        stack = new LIFO<>();
    }

    @Test
    void removeTest() {
        assertThrows(NoSuchElementException.class, stack::remove);
        stack.offer(1);
        stack.offer(2);
        assertEquals(new Integer(2), stack.remove());
        assertEquals(1, stack.size());
    }

    @Test
    void pollTest() {
        assertNull(stack.poll());
        stack.offer(1);
        stack.offer(2);
        assertEquals(new Integer(2), stack.poll());
        assertEquals(1, stack.size());
    }

    @Test
    void elementTest() {
        assertThrows(NoSuchElementException.class, stack::element);
        stack.offer(1);
        stack.offer(2);
        assertEquals(new Integer(2), stack.element());
        assertEquals(2, stack.size());
    }

    @Test
    void peekTest() {
        assertNull(stack.peek());
        stack.offer(1);
        stack.offer(2);
        assertEquals(new Integer(2), stack.peek());
        assertEquals(2, stack.size());
    }

    @Test
    void offerTest() {
        MyQueueTest.offerTest(stack);
    }


    /**Unsupported methods*/
    @Test
    void containsTest() {
        MyQueueTest.containsTest(stack);
    }

    @Test
    void toObjectArrayTest() {
        MyQueueTest.toObjectArrayTest(stack);
    }

    @Test
    void toGenericArrayTest() {
        MyQueueTest.toGenericArrayTest(stack);
    }

    @Test
    void removeObjectTest() {
        MyQueueTest.removeObjectTest(stack);
    }

    @Test
    void containsAllTest() {
        MyQueueTest.containsAllTest(stack);
    }

    @Test
    void removeAllTest() {
        MyQueueTest.removeAllTest(stack);
    }

    @Test
    void retainAllTest() {
        MyQueueTest.retainAllTest(stack);
    }


    /**Unnecessary tests, since LinkedList is used and has those same methods we may assume that they work properly*/
    @Test
    void sizeTest() {
        MyQueueTest.sizeTest(stack);
    }

    @Test
    void isEmptyTest() {
        MyQueueTest.isEmptyTest(stack);
    }

    @Test
    void iteratorTest() {
        MyQueueTest.iteratorTest(stack);
    }

    @Test
    void addTest() {
        MyQueueTest.addTest(stack);
    }

    @Test
    void addAllTest() {
        MyQueueTest.addAllTest(stack);
        MyQueueTest.addAllSubclassTest(new LIFO<>());

        LIFO<LIFO> queueAddAllTest = new LIFO<>();
        ArrayList<LIFO> testList = new ArrayList<>();
        testList.add(new LIFO<>());
        testList.add(new LIFO());
        testList.add(new LIFO());
        queueAddAllTest.addAll(testList);
        assertEquals(3, queueAddAllTest.size());
    }

    @Test
    void clearTest() {
        MyQueueTest.clearTest(stack);
    }

}