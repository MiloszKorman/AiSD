package queues;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class MyQueueTest {

    static void offerTest(MyQueue<Integer> queue) {
        queue.offer(1);
        assertEquals(1, queue.size());
        assertEquals(new Integer(1), queue.poll());
        queue.offer(null);
        assertEquals(1, queue.size());
    }

    static void isEmptyTest(MyQueue<Integer> queue) {
        assertTrue(queue.isEmpty());
        queue.offer(1);
        assertFalse(queue.isEmpty());
        queue.remove();
        assertTrue(queue.isEmpty());
    }


    /**Unsupported methods*/
    static void containsTest(MyQueue<Integer> queue) {
        queue.offer(1);
        queue.offer(2);
        assertThrows(UnsupportedOperationException.class, () -> queue.contains(1));
    }

    static void toObjectArrayTest(MyQueue<Integer> queue) {
        queue.offer(1);
        queue.offer(2);
        assertThrows(UnsupportedOperationException.class, queue::toArray);
    }

    static void toGenericArrayTest(MyQueue<Integer> queue) {
        queue.offer(1);
        queue.offer(2);
        assertThrows(UnsupportedOperationException.class, () -> queue.toArray(new Integer[2]));
    }

    static void removeObjectTest(MyQueue<Integer> queue) {
        queue.offer(1);
        queue.offer(2);
        assertThrows(UnsupportedOperationException.class, () -> queue.remove(2));
    }

    static void containsAllTest(MyQueue<Integer> queue) {
        queue.offer(1);
        queue.offer(2);
        HashSet<Integer> testSet = new HashSet<>();
        testSet.add(1);
        testSet.add(2);
        assertThrows(UnsupportedOperationException.class, () -> queue.containsAll(testSet));
    }

    static void removeAllTest(MyQueue<Integer> queue) {
        queue.offer(1);
        queue.offer(2);
        queue.offer(3);
        HashSet<Integer> testSet = new HashSet<>();
        testSet.add(1);
        testSet.add(2);
        assertThrows(UnsupportedOperationException.class, () -> queue.removeAll(testSet));
    }

    static void retainAllTest(MyQueue<Integer> queue) {
        queue.offer(1);
        queue.offer(2);
        queue.offer(3);
        queue.offer(4);
        HashSet<Integer> testSet = new HashSet<>();
        testSet.add(1);
        testSet.add(2);
        assertThrows(UnsupportedOperationException.class, () -> queue.retainAll(testSet));
    }


    /**Unnecessary tests, since LinkedList is used and has those same methods we may assume that they work properly*/
    static void sizeTest(MyQueue<Integer> queue) {
        //check whether size of an empty list is 0
        assertEquals(0, queue.size());

        //check if size is properly calculated
        queue.offer(1);
        queue.offer(2);
        assertEquals(2, queue.size());

        //check if it changes after removal of an element
        queue.remove();
        assertEquals(1, queue.size());
    }

    static void iteratorTest(MyQueue<Integer> queue) {
        queue.clear();

        for(int i = 1; i <= 15; i++) {
            queue.offer(i);
        }

        int i = 1;
        for(Integer element : queue) {
            assertEquals(new Integer(i), element);
            i++;
        }

        assertEquals(16, i);
    }

    static void addTest(MyQueue<Integer> queue) {
        //checks whether element is indeed added to the list
        queue.add(1);
        assertEquals(1, queue.size());
        assertEquals(new Integer(1), queue.poll());

        //method should accept nulls
        queue.clear();
        queue.add(null);
        assertEquals(1, queue.size());
        assertNull(queue.poll());
    }

    static void addAllTest(MyQueue<Integer> queue) {

        //check whether after adding a collection size is properly calculated
        HashSet<Integer> testSet = new HashSet<>();
        testSet.add(1);
        testSet.add(2);
        testSet.add(3);
        queue.addAll(testSet);
        assertEquals(3, queue.size());

        //check if trying to add empty collection causes false
        assertFalse(queue.addAll(new HashSet<>()));

        //null pointer exception should be thrown when provided collection is null
        assertThrows(NullPointerException.class, () -> queue.addAll(null));
    }

    //XDDD
    static void addAllSubclassTest(MyQueue<MyQueue> queue) {
        ArrayList<MyQueue> testList = new ArrayList<>();
        testList.add(new FIFO());
        testList.add(new LIFO());
        testList.add(new FIFO());
        queue.addAll(testList);
        assertEquals(3, queue.size());
        assertTrue(queue.peek() instanceof FIFO);
    }

    static void clearTest(MyQueue<Integer> queue) {
        //check whether list is cleared correctly
        queue.offer(1);
        queue.offer(2);
        queue.offer(3);
        queue.clear();
        assertEquals(0, queue.size());
    }

}