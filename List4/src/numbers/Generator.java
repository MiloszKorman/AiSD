package numbers;

public class Generator {

    /**
     * When this method is called, it creates an array of provided size filled with integers in range [-100, 100]
     *
     * @param howMany - integer specifying size of returned array
     * @return Array of integers
     */
    public static int[] generateArrayOfRandomNumbers(int howMany) {

        int[] array = new int[howMany];

        for (int i = 0; i < howMany; i++) {
            array[i] = (int) (Math.random() * 201) - 100;
        }

        return array;
    }

}
