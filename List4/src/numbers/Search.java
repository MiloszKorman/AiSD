package numbers;

import java.util.Stack;
import java.util.function.Consumer;

public class Search {

    private int[] mainArray;
    private Stack<Integer> helper = new Stack<>();
    private Consumer<int[]> sort;

    public Search(int[] array, Consumer<int[]> sort) {
        this.mainArray = array;
        this.sort = sort;
        sort.accept(mainArray);
    }

    /**
     * When this method is called, it prints out index or range of indexes, on which provided element has been found.
     * If element is not in the collection, suitable statement is printed.
     *
     * @param element - integer that is supposed to be found in collection
     */
    public void binarySearch(int element) {

        if (!helper.empty()) {
            addAllPendingNumbers();
        }

        int beginning = 0;
        int end = mainArray.length - 1;
        int middle = (mainArray.length - 1) / 2;

        while (mainArray[middle] != element && beginning <= end) {
            if (element < mainArray[middle]) {
                end = middle - 1;
            } else {
                beginning = middle + 1;
            }
            middle = (beginning + end) / 2;
        }

        if (mainArray[middle] == element) {
            int startingIndex = middle;
            while (0 < startingIndex && mainArray[startingIndex - 1] == element) {
                startingIndex--;
            }
            int endingIndex = startingIndex;
            while (endingIndex < mainArray.length - 1 && mainArray[endingIndex + 1] == element) {
                endingIndex++;
            }

            if (startingIndex == endingIndex) {
                System.out.print("Element " + element + " can be found on index: " + startingIndex);
            } else {
                System.out.print("Element " + element + " can be found on indexes: " + startingIndex + " to " + endingIndex);
            }

        } else {
            System.out.println("No such element has been found");
        }

        System.out.println();

    }

    /**
     * When this method is called, it adds provided number in suitable place in the collection
     *
     * @param numberToAdd - number to be added
     */
    public void add(int numberToAdd) {
        helper.push(numberToAdd);
    }

    private void addAllPendingNumbers() {

        int[] newArray = new int[mainArray.length + helper.size()];

        int i = 0;
        for (; i < mainArray.length; i++) {
            newArray[i] = mainArray[i];
        }

        for (; i < newArray.length; i++) {
            newArray[i] = helper.peek();
        }

        mainArray = newArray;
        sort.accept(mainArray);
    }

}
