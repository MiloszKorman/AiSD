package numbers;

public class Sort {

    /**
     * When this method is called, it sorts provided array accordingly to bucket sort technique, creates array of size
     * biggest element - smallest element in provided array + 1, on each index in this array, times of occurences of
     * indexes number in original array is stored, which allows to recreate original array, but sorted from smallest
     * to largest values
     *
     * @param arrToSort - array which is ought to be sorted
     */
    public static void bucketSort(int[] arrToSort) {

        int smallest = Integer.MAX_VALUE;
        int largest = Integer.MIN_VALUE;

        for (int iElement = 0; iElement < arrToSort.length; iElement++) {
            if (arrToSort[iElement] < smallest) {
                smallest = arrToSort[iElement];
            }
            if (largest < arrToSort[iElement]) {
                largest = arrToSort[iElement];
            }
        }

        int[] occurrenceArray = new int[largest - smallest + 1];

        for (int iElement = 0; iElement < arrToSort.length; iElement++) {
            occurrenceArray[arrToSort[iElement] - smallest]++;
        }

        int iElement = 0;
        for (int element = 0; element < occurrenceArray.length; element++) {
            while (0 < occurrenceArray[element]) {
                arrToSort[iElement] = element + smallest;
                occurrenceArray[element]--;
                iElement++;
            }
        }

    }

    /**
     * When this method is called, it sorts provided array accordingly to selection sort technique, om each index, if
     * any smaller element is found on further index, then element on current index, and smallest, on any bigger index,
     * get swapped out
     *
     * @param arrToSort - array which is ought to be sorted
     */
    public static void selectionSort(int[] arrToSort) {

        for (int iElement = 0; iElement < arrToSort.length - 1; iElement++) {

            int iSmallestFurther = iElement;

            for (int iLaterElement = iElement + 1; iLaterElement < arrToSort.length; iLaterElement++) {
                if (arrToSort[iLaterElement] < arrToSort[iSmallestFurther]) {
                    iSmallestFurther = iLaterElement;
                }
            }

            if(iElement < iSmallestFurther) {
                int holder = arrToSort[iElement];
                arrToSort[iElement] = arrToSort[iSmallestFurther];
                arrToSort[iSmallestFurther] = holder;
            }

        }

    }

}
