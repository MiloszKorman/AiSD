package runner;

import numbers.Sort;
import numbers.Search;
import numbers.Generator;

public class Run {

    public static void main(String[] args) {

        Search mySearch = new Search(Generator.generateArrayOfRandomNumbers(1000000), Sort::bucketSort);
        mySearch.binarySearch((int) (Math.random() * 201) - 100);

    }

}
