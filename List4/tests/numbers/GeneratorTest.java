package numbers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GeneratorTest {

    //check whether array of correct size is created, and whether values are in correct range
    @Test
    void generateArrayOfRandomNumbersTest() {
        int[] array = Generator.generateArrayOfRandomNumbers(1000000);
        assertEquals(1000000, array.length);
        Sort.bucketSort(array);
        assertEquals(-100, array[0]);
        assertEquals(100, array[array.length - 1]);
    }

}