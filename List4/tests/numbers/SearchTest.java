package numbers;

import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class SearchTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    //check whether binary search works well with searching for positive numbers, negative numbers,
    //element that is not in a collection, element present on few indexes
    @Test
    void binarySearchTest() {

        int[] array1 = {6, 2, -10, 5};
        Search mySearch1 = new Search(array1, Sort::bucketSort);
        mySearch1.binarySearch(2);
        String[] messages = outContent.toString().split("\n");
        assertEquals("Element 2 can be found on index: 1", messages[messages.length - 1]);
        mySearch1.binarySearch(-10);
        messages = outContent.toString().split("\n");
        assertEquals("Element -10 can be found on index: 0", messages[messages.length - 1]);
        mySearch1.binarySearch(4);
        messages = outContent.toString().split("\n");
        assertEquals("No such element has been found", messages[messages.length - 1]);

        int[] array2 = {0, 0, 5, 4, 0};
        Search mySearch2 = new Search(array2, Sort::selectionSort);
        mySearch2.binarySearch(0);
        messages = outContent.toString().split("\n");
        assertEquals("Element 0 can be found on indexes: 0 to 2", messages[messages.length - 1]);

    }

    //check whether after adding a new element, all elements are available and stored in correct order
    @Test
    void addTest() {

        int[] array = {6, 2, -10, 5};
        Search mySearch = new Search(array, Sort::selectionSort);
        mySearch.binarySearch(6);
        String[] messages = outContent.toString().split("\n");
        assertEquals("Element 6 can be found on index: 3", messages[messages.length - 1]);
        mySearch.add(1);
        mySearch.binarySearch(1);
        messages = outContent.toString().split("\n");
        assertEquals("Element 1 can be found on index: 1", messages[messages.length - 1]);

    }

    @AfterEach
    public void end() {
        System.setOut(System.out);
    }

}