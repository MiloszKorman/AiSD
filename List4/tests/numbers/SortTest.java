package numbers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortTest {

    @Test
    void bucketSortTest() {
        int[] positiveArray = createPositiveNumbersArray();
        Sort.bucketSort(positiveArray);
        checkPositiveArraySort(positiveArray);

        int[] negativeArray = createNegativeNumbersArray();
        Sort.bucketSort(negativeArray);
        checkNegativeArraySort(negativeArray);

        int[] mixedArray = createMixedArray();
        Sort.bucketSort(mixedArray);
        checkMixedArraySort(mixedArray);
    }

    @Test
    void selectionSortTest() {
        int[] positiveArray = createPositiveNumbersArray();
        Sort.selectionSort(positiveArray);
        checkPositiveArraySort(positiveArray);

        int[] negativeArray = createNegativeNumbersArray();
        Sort.selectionSort(negativeArray);
        checkNegativeArraySort(negativeArray);

        int[] mixedArray = createMixedArray();
        Sort.selectionSort(mixedArray);
        checkMixedArraySort(mixedArray);
    }

    private int[] createPositiveNumbersArray() {
        int[] array = {4, 91, 213, 5, 25, 0, 1, 54, 100, 3, 432, 12, 7};
        return array;
    }

    private void checkPositiveArraySort(int[] array) {
        assertEquals(0, array[0]);
        assertEquals(1, array[1]);
        assertEquals(3, array[2]);
        assertEquals(4, array[3]);
        assertEquals(5, array[4]);
        assertEquals(7, array[5]);
        assertEquals(12, array[6]);
        assertEquals(25, array[7]);
        assertEquals(54, array[8]);
        assertEquals(91, array[9]);
        assertEquals(100, array[10]);
        assertEquals(213, array[11]);
        assertEquals(432, array[12]);
    }

    private int[] createNegativeNumbersArray() {
        int[] array = {-32, -1, -5, -420, -712, -5, -43};
        return array;
    }

    private void checkNegativeArraySort(int[] array) {
        assertEquals(-712, array[0]);
        assertEquals(-420, array[1]);
        assertEquals(-43, array[2]);
        assertEquals(-32, array[3]);
        assertEquals(-5, array[4]);
        assertEquals(-5, array[5]);
        assertEquals(-1, array[6]);
    }

    private int[] createMixedArray() {
        int[] array = {52, 1, 0, -4, 13, -78, 32, -5};
        return array;
    }

    private void checkMixedArraySort(int[] array) {
        assertEquals(-78, array[0]);
        assertEquals(-5, array[1]);
        assertEquals(-4, array[2]);
        assertEquals(0, array[3]);
        assertEquals(1, array[4]);
        assertEquals(13, array[5]);
        assertEquals(32, array[6]);
        assertEquals(52, array[7]);
    }

}