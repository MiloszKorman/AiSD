package sort;

import java.util.List;
import java.util.ListIterator;

public class Sorter {

    /**
     * Sorts provided list in place according to merge sort procedure
     *
     * @param listToSort provided list that is sorted in place
     */
    public static <T extends Comparable<? super T>> void mergeSort(List<T> listToSort) {
        if (!listToSort.isEmpty()) {
            Object[] arrHelper = listToSort.toArray();
            mergeSort(arrHelper, 0, arrHelper.length - 1);
            arrToList(listToSort, arrHelper);
        }
    }

    /**
     * Sorts specified part of provided array.
     *
     * @param arrToSort array that's ought to be sorted
     * @param left      index specifying start of a piece of an array that we should sort
     * @param right     index specifying end of a piece of an array that we should sort
     */
    private static void mergeSort(Object[] arrToSort, int left, int right) {
        if (left < right) {
            int middle = left + (right - left) / 2;

            mergeSort(arrToSort, left, middle);
            mergeSort(arrToSort, middle + 1, right);
            merge(arrToSort, left, middle, right);
        }
    }

    /**
     * Merges (sorts on left-right) two restricted areas of an array, specified by left, middle, right params.
     *
     * @param arr    array that's being sorted
     * @param left   index specifying start of first piece of an array that we will merge
     * @param middle index specifying end of first piece, and start of second piece of an array that we will merge
     * @param right  index specifying end of second piece of an array that we will merge
     */
    @SuppressWarnings("unchecked")
    private static void merge(Object[] arr, int left, int middle, int right) {

        Object[] leftArr = new Object[middle - left + 1];
        Object[] rightArr = new Object[right - middle - 1 + 1];

        for (int i = 0; i < leftArr.length; i++) {
            leftArr[i] = arr[left + i];
        }
        for (int i = 0; i < rightArr.length; i++) {
            rightArr[i] = arr[middle + 1 + i];
        }

        int iLeft = 0;
        int iRight = 0;
        int iArr = left;

        while (iLeft < leftArr.length && iRight < rightArr.length) {
            if (((Comparable) leftArr[iLeft]).compareTo(rightArr[iRight]) <= 0) {
                arr[iArr] = leftArr[iLeft];
                iLeft++;
            } else {
                arr[iArr] = rightArr[iRight];
                iRight++;
            }
            iArr++;
        }

        while (iLeft < leftArr.length) {
            arr[iArr] = leftArr[iLeft];
            iArr++;
            iLeft++;
        }

        while (iRight < rightArr.length) {
            arr[iArr] = rightArr[iRight];
            iArr++;
            iRight++;
        }

    }


    /**
     * Sorts provided list in place according to quick sort procedure
     *
     * @param listToSort provided list that is sorted in place
     */
    public static <T extends Comparable<? super T>> void quickSort(List<T> listToSort) {
        if (!listToSort.isEmpty()) {
            Object[] arrHelper = listToSort.toArray();
            quickSort(arrHelper, 0, arrHelper.length - 1);
            arrToList(listToSort, arrHelper);
        }
    }

    /**
     * Sorts specified part of provided array.
     *
     * @param arrToSort array that's ought to be sorted
     * @param left      index specifying start of a piece of an array that we should sort
     * @param right     index specifying end of a piece of an array that we should sort
     */
    private static void quickSort(Object[] arrToSort, int left, int right) {
        if (left < right) {
            int pi = partition(arrToSort, left, right);

            quickSort(arrToSort, left, pi - 1);
            quickSort(arrToSort, pi + 1, right);
        }
    }

    /**
     * Draws random element as pivot, puts it on the end, puts all smaller elements next to each other, then puts pivot
     * at the end of this sequence(not necessarily end of restricted area on array) and return index of pivot (where
     * it's placed at the end of the method)
     *
     * @param arr   provided array on which we operate
     * @param left  index specifying start of a piece of an array that we should sort
     * @param right index specifying end of a piece of an array that we should sort
     */
    @SuppressWarnings("unchecked")
    private static int partition(Object[] arr, int left, int right) {

        int pi = (int) (Math.random() * ((right - 1) - left + 1)) + left;

        swap(arr, pi, right);

        int smaller = left;

        for (int i = left; i < right; i++) {
            if (((Comparable) arr[i]).compareTo(arr[right]) <= 0) {
                swap(arr, smaller, i);
                smaller++;
            }
        }

        swap(arr, smaller, right);

        return smaller;
    }

    /**
     * Swaps two elements positions in array
     *
     * @param arr array in which we should swap to elements
     * @param one first element to be swapped
     * @param two secon element to be swapped
     */
    private static void swap(Object[] arr, int one, int two) {
        Object holder = arr[two];
        arr[two] = arr[one];
        arr[one] = holder;
    }

    /**
     * Copies objects from sorted array to unsorted list, that's ought to be sorted
     *
     * @param arr  provided array from which we take objects
     * @param list provided list to which we should move objects
     */
    @SuppressWarnings("unchecked")
    private static <T> void arrToList(List<T> list, Object[] arr) {
        ListIterator<T> iter = list.listIterator();
        for (Object elem : arr) {
            iter.next();
            iter.set((T) elem);
        }
    }

}
