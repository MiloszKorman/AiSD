package sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SorterTest {

    @Test
    void mergeSortTest() {
        List<Integer> list = Arrays.asList(12, 11, 13, 5, 6, 7);
        Sorter.mergeSort(list);
        assertEquals(Arrays.asList(5, 6, 7, 11, 12, 13), list);

        List<Integer> list1 = Arrays.asList(13, -5, 0);
        Sorter.mergeSort(list1);
        assertEquals(Arrays.asList(-5, 0, 13), list1);
    }

    @Test
    void quickSortTest() {
        List<Integer> list = Arrays.asList(12, 11, 13, 5, 6, 7);
        Sorter.quickSort(list);
        assertEquals(Arrays.asList(5, 6, 7, 11, 12, 13), list);

        List<Integer> list1 = Arrays.asList(13, -5, 0);
        Sorter.quickSort(list1);
        assertEquals(Arrays.asList(-5, 0, 13), list1);
    }

}