package map.hash_table;

import java.util.*;

public abstract class HashTable<K, V> implements Map<K, V> {

    protected int count = 0;

    protected Entry<?, ?>[] table;

    protected float threshold;
    protected float loadFactor;

    protected final int MAX_ARRAY_SIZE = 1000000;

    protected Prime prime;

    public HashTable(int initialCapacity, float loadFactor) {
        if (initialCapacity <= 0) {
            initialCapacity = 11;
        }
        if (loadFactor <= 0) {
            loadFactor = 0.75f;
        }
        table = new Entry<?, ?>[initialCapacity];
        this.loadFactor = loadFactor;
        this.threshold = table.length * loadFactor;
        prime = new Prime(initialCapacity);
    }

    /**
     * Checks whether there is an entry with provided key
     *
     * @param key key we check whether is in a hashtable
     * @return boolean value whether there is entry with that key
     * */
    @SuppressWarnings("unchecked")
    @Override
    public boolean containsKey(Object key) {
        return (findEntryByKey((K) key) != null);
    }

    /**
     * Looks for entry with provided value, returns whether there is any
     *
     * @param value for which hashtable is searched
     * @return boolean value whether entry with provided value has been found
     * */
    @Override
    public abstract boolean containsValue(Object value);

    /**
     * Searches for entry with provided key. If found then returns stored value, otherwise returns null
     *
     * @param key key we check whether is in a hashtable
     * @return value that has been previously under this key
     * */
    @SuppressWarnings("unchecked")
    @Override
    public V get(Object key) {
        Entry<K, V> entry = findEntryByKey((K) key);
        if (entry != null) {
            return entry.value;
        }

        return null;
    }

    /**
     * Searches for entry with provided key, if one is found old value is replaced for new value, and returned
     * Otherwise new entry is created, and null returned
     *
     * @param key by which we search hashtable
     * @param value which is ought to be placed
     * @return old value stored under this key or null if entry with such key hasn't been found
     */
    @Override
    public abstract V put(K key, V value);

    /**
     * @return how many entries there are
     * */
    @Override
    public int size() {
        return count;
    }

    /**
     * @return boolean value whether hashtable has no entries
     **/
    @Override
    public boolean isEmpty() {
        return (count == 0);
    }

    /**
     * Copies all entries from provided map to hashtable
     *
     * @param m map to inject
     */
    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> entryToPut : m.entrySet()) {
            put(entryToPut.getKey(), entryToPut.getValue());
        }
    }

    /**
     * Finds out new indexes for all the entries, based on their hashes, after the table has been expanded
     */
    protected abstract void rehash();

    /**
     * Searches for entry with provided key, if one is found it is remove, and it's value is returned. otherwise null
     * is returned
     *
     * @param key specifying which entry should be removed
     * @return old value if entry with such key has been found, otherwise null
     */
    @Override
    public abstract V remove(Object key);

    /**
     * Searches for entry stored by provided key, if found then checks whether values match, if so, removes this entry
     *
     * @param key by which we try to find the entry
     * @param value which the entry should contain
     * @return boolean value whether entry which such key and value has been found, if so, whether it has been removed
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean remove(Object key, Object value) {

        if (value == null) {
            throw new NullPointerException();
        }

        Entry<K, V> entry = findEntryByKey((K) key);
        if (entry != null) {
            if (entry.value.equals(value)) {
                remove(key);
                return true;
            }
        }

        return false;
    }

    /**
     * Searches for entry with provided key, if found and value matches provided oldValue, newValue is placed into entry
     *
     * @param key by which entry is searched for
     * @param oldValue which entry should contain
     * @param newValue which is placed when requirements are met
     * @return boolean value whether entry with such key and value was found
     */
    @Override
    public boolean replace(K key, V oldValue, V newValue) {

        if (oldValue == null || newValue == null) {
            throw new NullPointerException();
        }

        Entry<K, V> entry = findEntryByKey(key);
        if (entry != null) {
            if (entry.value.equals(oldValue)) {
                entry.value = newValue;
                return true;
            }
        }

        return false;
    }

    /**
     * Searches for entry with provided key, if found then places provided value inside and returns old value, otherwise
     * return null
     *
     * @param key by which we look for the entry
     * @param value ought to be placed to the entry
     * @return old value, if entry found, otherwise null
     */
    @Override
    public V replace(K key, V value) {

        if (value == null) {
            throw new NullPointerException();
        }

        Entry<K, V> entry = findEntryByKey(key);
        if (entry == null) {
            return null;
        } else {
            V old = entry.value;
            entry.value = value;
            return old;
        }
    }

    /**
     * Restores to default initial values
     */
    @Override
    public void clear() {
        table = new Entry<?, ?>[11];
        prime = new Prime(11);
        count = 0;
        threshold = (table.length * loadFactor);
    }

    /**
     * Searches for entry with provided key, if found returns value stored, otherwise returns provided default value
     *
     * @param key by which entry is searched for
     * @param defaultValue returned when entry is not found
     * @return entries value or default value
     */
    @Override
    public V getOrDefault(Object key, V defaultValue) {
        V value = get(key);

        if (value == null) {
            return defaultValue;
        } else {
            return value;
        }
    }

    /**
     * Searches for entry with provided key, if one is not found, new entry is placed with provided key and value
     *
     * @param key by which entry is searched for
     * @param value which is ought to be placed
     * @return entries value when found, otherwise null
     */
    @Override
    public V putIfAbsent(K key, V value) {

        if (value == null) {
            throw new NullPointerException();
        }

        V oldValue = get(key);

        if (oldValue == null) {
            put(key, value);
        }

        return oldValue;
    }

    /**
     * Searches through table for entry with provided key
     *
     * @param key by which entry is being searched for
     * @return found entry or null
     */
    protected abstract Entry<K, V> findEntryByKey(K key);

    /**
     * Returns iterable set of keys
     *
     * @return set of keys stored in the table
     */
    @Override
    public abstract Set<K> keySet();

    /**
     * Returns iterable collection of values
     *
     * @return collection of values stored in the table
     */
    @Override
    public abstract Collection<V> values();

    /**
     * Returns iterable set of entries (key and value)
     *
     * @return set of entries (key and value) stored in the table
     */
    @Override
    public abstract Set<Map.Entry<K, V>> entrySet();

    class KeySet extends AbstractSet<K> {

        Iterator<K> keyIterator;

        KeySet(Iterator<K> keyIterator) {
            this.keyIterator = keyIterator;
        }

        @Override
        public Iterator<K> iterator() {
            return keyIterator;
        }

        @Override
        public int size() {
            return count;
        }

    }

    class KeyIterator<T> implements Iterator<T> {

        Iterator<T> entryIterator;

        KeyIterator(Iterator<T> entryIterator) {
            this.entryIterator = entryIterator;
        }

        @Override
        public boolean hasNext() {
            return entryIterator.hasNext();
        }

        @SuppressWarnings("unchecked")
        @Override
        public T next() {
            Entry<?, ?> entry = (Entry<?, ?>) entryIterator.next();

            if (entry != null) {
                return (T) entry.key;
            }

            return null;
        }

    }

    class ValueCollection extends AbstractCollection<V> {

        Iterator<V> valueIterator;

        ValueCollection(Iterator<V> valueIterator) {
            this.valueIterator = valueIterator;
        }

        @Override
        public Iterator<V> iterator() {
            return valueIterator;
        }

        @Override
        public int size() {
            return count;
        }

    }

    class ValueIterator<T> implements Iterator<T> {

        Iterator<T> entryIterator;

        ValueIterator(Iterator<T> entryIterator) {
            this.entryIterator = entryIterator;
        }

        @Override
        public boolean hasNext() {
            return entryIterator.hasNext();
        }

        @SuppressWarnings("unchecked")
        @Override
        public T next() {
            Entry<?, ?> entry = (Entry<?, ?>) entryIterator.next();

            if (entry != null) {
                return (T) entry.value;
            }

            return null;
        }

    }

    class EntrySet extends AbstractSet<Map.Entry<K, V>> {

        Iterator<Map.Entry<K, V>> entryIterator;

        EntrySet(Iterator<Map.Entry<K, V>> entryIterator) {
            this.entryIterator = entryIterator;
        }

        @Override
        public Iterator<Map.Entry<K, V>> iterator() {
            return entryIterator;
        }

        @Override
        public int size() {
            return count;
        }

    }

    abstract class EntryIterator<T> implements Iterator<T> {

        Entry<?, ?> lastEntry;
        int currentIndex = 0;

        @SuppressWarnings("unchecked")
        @Override
        public boolean hasNext() {
            for (int i = currentIndex + 1; i < table.length; i++) {
                if (table[i] != null) {
                    currentIndex = i - 1;
                    lastEntry = table[currentIndex];
                    return true;
                }
            }

            return false;
        }

    }

    static class Entry<K, V> implements Map.Entry<K, V> {

        int hash;
        K key;
        V value;

        Entry(int hash, K key, V value) {
            this.hash = hash;
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;

            return oldValue;
        }

    }

    class Prime {

        PrimeNumber currentPrime = new PrimeNumber(11,
                new PrimeNumber(17,
                        new PrimeNumber(29,
                                new PrimeNumber(37,
                                        new PrimeNumber(47,
                                                new PrimeNumber(59,
                                                        new PrimeNumber(67,
                                                                new PrimeNumber(79,
                                                                        new PrimeNumber(89,
                                                                                new PrimeNumber(101, null))))))))));

        Prime(int initialCapacity) {
            while (currentPrime.value < initialCapacity) {
                next();
            }
        }

        int next() {
            if (currentPrime.next == null) {
                currentPrime = new PrimeNumber(findNextPrime(), null);
            } else {
                currentPrime = currentPrime.next;
            }
            return currentPrime.value;
        }

        private int findNextPrime() {
            for (int i = currentPrime.value; i < MAX_ARRAY_SIZE; i+=2) {
                if (pseudo_math.CheckPrime.isPrime(i)) {
                    return i;
                }
            }
            return MAX_ARRAY_SIZE;
        }

        private class PrimeNumber {

            int value;
            PrimeNumber next;

            PrimeNumber(int value, PrimeNumber next) {
                this.value = value;
                this.next = next;
            }

        }

    }

}
