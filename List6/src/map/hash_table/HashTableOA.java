package map.hash_table;

import java.util.*;

public class HashTableOA<K, V> extends HashTable<K, V> {

    public HashTableOA(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public HashTableOA(int initialCapacity) {
        this(initialCapacity, 0.75f);
    }

    public HashTableOA() {
        this(11);
    }

    @Override
    public boolean containsValue(Object value) {

        if (value == null) {
            return false;
        }

        for (Entry<?, ?> entry : table) {
            if (entry != null && entry.value.equals(value)) {
                return true;
            }
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public V put(K key, V value) {

        if (value == null) {
            throw new NullPointerException();
        }

        int hash = key.hashCode();

        for (int i = 1; i <= table.length; i++) {
            int index = (Math.abs(hash) + i * i) % table.length;
            Entry<K, V> entry = (Entry<K, V>) table[index];

            if (entry == null) {
                if (threshold <= count) {
                    rehash();
                }
                index = (Math.abs(hash) + i * i) % table.length;
                table[index] = new Entry<>(hash, key, value);
                count++;
                return null;
            } else if ((entry.hash == hash) || (entry.key.equals(key))) {
                V old = entry.value;
                entry.value = value;
                return old;
            }
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void rehash() {
        int newCapacity = table.length;

        if (table.length != MAX_ARRAY_SIZE) {

            while (newCapacity * loadFactor <= threshold && newCapacity < MAX_ARRAY_SIZE) {
                newCapacity = prime.next();
            }

            Entry<?, ?>[] newTable = new Entry<?, ?>[newCapacity];

            threshold = (int) (newTable.length * loadFactor);

            for (Entry<K, V> entryToCopy : (Entry<K, V>[]) table) {
                if (entryToCopy != null) {
                    for (int j = 1; j <= table.length; j++) {
                        int index = (Math.abs(entryToCopy.hash) + j * j) % newTable.length;
                        if (newTable[index] == null) {
                            newTable[index] = entryToCopy;
                            break;
                        }
                    }
                }
            }

            table = newTable;

        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public V remove(Object key) {

        int hash = key.hashCode();

        for (int i = 1; i <= table.length; i++) {
            int index = (Math.abs(hash) + i * i) % table.length;
            Entry<K, V> entry = (Entry<K, V>) table[index];
            if (entry != null && entry.hash == hash && entry.key.equals(key)) {
                V old = entry.value;
                table[index] = null;
                count--;
                return old;
            }
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<K> keySet() {
        return new KeySet(new KeyIterator(new EntryIteratorOA()));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<V> values() {
        return new ValueCollection(new ValueIterator(new EntryIteratorOA()));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return new EntrySet(new EntryIteratorOA());
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Entry<K, V> findEntryByKey(K key) {

        int hash = key.hashCode();

        for (int i = 1; i <= table.length; i++) {
            int index = (Math.abs(hash) + i * i) % table.length;
            if (table[index] != null && (table[index].hash == hash) && (table[index].key.equals(key))) {
                return (Entry<K, V>) table[index];
            }
        }

        return null;
    }

    class EntryIteratorOA<T> extends EntryIterator<T> {

        @SuppressWarnings("unchecked")
        @Override
        public T next() {
            if (hasNext()) {
                currentIndex++;
                lastEntry = table[currentIndex];
                return (T) lastEntry;
            }

            return null;
        }

    }

}
