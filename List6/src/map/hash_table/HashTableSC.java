package map.hash_table;

import java.util.*;

public class HashTableSC<K, V> extends HashTable<K, V> {

    public HashTableSC(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public HashTableSC(int initialCapacity) {
        this(initialCapacity, 0.75f);
    }

    public HashTableSC() {
        this(11);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean containsValue(Object value) {

        if (value == null) {
            return false;
        }

        for (Entry<?, ?> genericEntry : table) {
            EntrySC<K, V> entry = (EntrySC<K, V>) genericEntry;
            for (; entry != null; entry = entry.next) {
                if (entry.value.equals(value)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public V put(K key, V value) {

        if (value == null) {
            throw new NullPointerException();
        }

        int hash = key.hashCode();
        int index = Math.abs(hash) % table.length;

        Entry<K, V> entry = findEntryByKey(key);
        if (entry != null) {
            V oldValue = entry.value;
            entry.value = value;
            return oldValue;
        } else {
            addEntry(hash, key, value, index);

            return null;
        }
    }

    /**
     * Puts new entry into the table, adds a reference to other entries on this place in the table
     *
     * @param hash by which index is calculated
     * @param key of entry
     * @param value which entry is supposed to store
     * @param index on which entry should be placed
     */
    @SuppressWarnings("unchecked")
    private void addEntry(int hash, K key, V value, int index) {

        if (threshold <= count) {
            rehash();

            index = Math.abs(hash) % table.length;
        }

        EntrySC<K, V> nextEntry = (EntrySC<K, V>) table[index];
        table[index] = new EntrySC<>(hash, key, value, nextEntry);
        count++;

    }

    @SuppressWarnings("unchecked")
    @Override
    protected void rehash() {
        int newCapacity = table.length;

        if (table.length != MAX_ARRAY_SIZE) {

            while (newCapacity * loadFactor <= threshold && newCapacity < MAX_ARRAY_SIZE) {
                newCapacity = prime.next();
            }

            Entry<?, ?>[] newTable = new Entry<?, ?>[newCapacity];

            threshold = (int) (newTable.length * loadFactor);

            for (Entry<K, V> genericEntryToCopy : (Entry<K, V>[]) table) {
                EntrySC<K, V> entryToCopy = (EntrySC<K, V>) genericEntryToCopy;
                for (; entryToCopy != null; entryToCopy = entryToCopy.next) {
                    int index = Math.abs(entryToCopy.hash) % newTable.length;
                    entryToCopy.next = (EntrySC<K, V>) newTable[index];
                    newTable[index] = entryToCopy;
                }
            }

            table = newTable;

        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public V remove(Object key) {
        int hash = key.hashCode();
        int index = Math.abs(hash) % table.length;

        V old = null;

        if (table[index] != null) {
            if (table[index].hash == hash && table[index].key.equals(key)) {
                old = (V) table[index].value;
                table[index] = ((EntrySC<?, ?>)table[index]).next;
                count--;
            } else {
                EntrySC<K, V> prevEntry = (EntrySC<K, V>) table[index];
                for (EntrySC<K, V> entry = prevEntry.next; entry != null; prevEntry = entry, entry = entry.next) {
                    if (entry.hash == hash && entry.key.equals(key)) {
                        old = entry.value;
                        prevEntry.next = entry.next;
                        count--;
                    }
                }
            }
        }

        return old;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<K> keySet() {
        return new KeySet(new KeyIterator(new EntryIteratorSC()));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<V> values() {
        return new ValueCollection(new ValueIterator(new EntryIteratorSC()));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return new EntrySet(new EntryIteratorSC());
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Entry<K, V> findEntryByKey(K key) {

        int hash = key.hashCode();
        int index = Math.abs(hash) % table.length;

        for (EntrySC<K, V> entry = (EntrySC<K, V>) table[index]; entry != null; entry = entry.next) {
            if ((entry.hash == hash) && entry.key.equals(key)) {
                return entry;
            }
        }

        return null;
    }

    static class EntrySC<K, V> extends Entry<K, V> {

        EntrySC<K, V> next;

        EntrySC(int hash, K key, V value, EntrySC<K, V> next) {
            super(hash, key, value);
            this.next = next;
        }
    }

    class EntryIteratorSC<T> extends EntryIterator<T> {

        @SuppressWarnings("unchecked")
        @Override
        public boolean hasNext() {

            if (lastEntry != null && ((EntrySC<?, ?>)lastEntry).next != null) {
                return true;
            }

            return super.hasNext();
        }

        @SuppressWarnings("unchecked")
        @Override
        public T next() {
            if (hasNext()) {
                if (lastEntry != null && ((EntrySC<?, ?>)lastEntry).next != null) {
                    lastEntry = ((EntrySC<?, ?>)lastEntry).next;
                    return (T) lastEntry;
                } else {
                    currentIndex++;
                    lastEntry = table[currentIndex];
                    return (T) lastEntry;
                }
            }

            return null;
        }

    }

}
