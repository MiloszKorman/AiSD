package map.hash_table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HashTableOATest {

    private HashTableOA<Integer, String> hashTableOA;

    @BeforeEach
    public void setUp() {
        hashTableOA = new HashTableOA<>();
    }

    @Test
    void sizeTest() {
        HashTableTest.sizeTest(hashTableOA);
    }

    @Test
    void isEmptyTest() {
        HashTableTest.isEmptyTest(hashTableOA);
    }

    @Test
    void containsKeyTest() {
        HashTableTest.containsKeyTest(hashTableOA);
    }

    @Test
    void containsValueTest() {
        HashTableTest.containsValueTest(hashTableOA);
    }

    @Test
    void getTest() {
        HashTableTest.getTest(hashTableOA);
    }

    @Test
    void putTest() {
        HashTableTest.putTest(hashTableOA);
    }

    @Test
    void removeTest() {
        HashTableTest.removeTest(hashTableOA);
    }

    @Test
    void putAllTest() {
        HashTableTest.putAllTest(hashTableOA);
    }

    @Test
    void clearTest() {
        HashTableTest.clearTest(hashTableOA);
    }

    @Test
    void keySetTest() {
        HashTableTest.keySetTest(hashTableOA);
    }

    @Test
    void valuesTest() {
        HashTableTest.valuesTest(hashTableOA);
    }

    @Test
    void entrySetTest() {
        HashTableTest.entrySetTest(hashTableOA);
    }

    @Test
    void getOrDefaultTest() {
        HashTableTest.getOrDefaultTest(hashTableOA);
    }

    @Test
    void putIfAbsentTest() {
        HashTableTest.putIfAbsentTest(hashTableOA);
    }

    @Test
    void removeIfValueTest() {
        HashTableTest.removeIfValueTest(hashTableOA);
    }

    @Test
    void replaceIfOldValueTest() {
        HashTableTest.replaceIfOldValueTest(hashTableOA);
    }

    @Test
    void replaceTest() {
        HashTableTest.replaceTest(hashTableOA);
    }

}