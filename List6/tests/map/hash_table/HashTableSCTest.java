package map.hash_table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HashTableSCTest {

    private HashTableSC<Integer, String> hashTableSC;

    @BeforeEach
    public void setUp() {
        hashTableSC = new HashTableSC<>();
    }

    @Test
    void sizeTest() {
        HashTableTest.sizeTest(hashTableSC);
    }

    @Test
    void isEmptyTest() {
        HashTableTest.isEmptyTest(hashTableSC);
    }

    @Test
    void containsKeyTest() {
        HashTableTest.containsKeyTest(hashTableSC);
    }

    @Test
    void containsValueTest() {
        HashTableTest.containsValueTest(hashTableSC);
    }

    @Test
    void getTest() {
        HashTableTest.getTest(hashTableSC);
    }

    @Test
    void putTest() {
        HashTableTest.putTest(hashTableSC);
    }

    @Test
    void removeTest() {
        HashTableTest.removeTest(hashTableSC);
    }

    @Test
    void putAllTest() {
        HashTableTest.putAllTest(hashTableSC);
    }

    @Test
    void clearTest() {
        HashTableTest.clearTest(hashTableSC);
    }

    @Test
    void keySetTest() {
        HashTableTest.keySetTest(hashTableSC);
    }

    @Test
    void valuesTest() {
        HashTableTest.valuesTest(hashTableSC);
    }

    @Test
    void entrySetTest() {
        HashTableTest.entrySetTest(hashTableSC);
    }

    @Test
    void getOrDefaultTest() {
        HashTableTest.getOrDefaultTest(hashTableSC);
    }

    @Test
    void putIfAbsentTest() {
        HashTableTest.putIfAbsentTest(hashTableSC);
    }

    @Test
    void removeIfValueTest() {
        HashTableTest.removeIfValueTest(hashTableSC);
    }

    @Test
    void replaceIfOldValueTest() {
        HashTableTest.replaceIfOldValueTest(hashTableSC);
    }

    @Test
    void replaceTest() {
        HashTableTest.replaceTest(hashTableSC);
    }

}