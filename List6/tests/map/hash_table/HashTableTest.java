package map.hash_table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HashTableTest {

    static void sizeTest(HashTable<Integer, String> hashTable) {
        assertEquals(0, hashTable.size());
        hashTable.put(1, "hey");
        assertEquals(1, hashTable.size());
        hashTable.put(1, "one");
        hashTable.put(2, "two");
        hashTable.clear();
        assertTrue(hashTable.isEmpty());
    }

    static void isEmptyTest(HashTable<Integer, String> hashTable) {
        assertTrue(hashTable.isEmpty());
        hashTable.put(1, "hey");
        assertFalse(hashTable.isEmpty());
    }

    static void containsKeyTest(HashTable<Integer, String> hashTable) {
        assertFalse(hashTable.containsKey(1));
        hashTable.put(1, "Hey");
        assertTrue(hashTable.containsKey(1));
    }

    static void containsValueTest(HashTable<Integer, String> hashTable) {
        assertFalse(hashTable.containsValue("Hey"));
        hashTable.put(1, "Hey");
        assertTrue(hashTable.containsValue("Hey"));
    }

    static void getTest(HashTable<Integer, String> hashTable) {
        assertNull(hashTable.get(15));
        hashTable.put(1, "Hey");
        assertEquals("Hey", hashTable.get(1));
        hashTable.put(-15, "Dude");
        assertEquals("Dude", hashTable.get(-15));
    }

    static void putTest(HashTable<Integer, String> hashTable) {
        assertEquals(0, hashTable.size());
        hashTable.put(1, "Hey");
        assertEquals(1, hashTable.size());
        assertTrue(hashTable.containsKey(1));
        assertTrue(hashTable.containsValue("Hey"));

        hashTable = new HashTableSC<>(2);
        hashTable.put(1, "one");
        hashTable.put(2, "two");
        hashTable.put(3, "three");
        hashTable.put(4, "four");
        assertEquals("one", hashTable.get(1));
        assertEquals("two", hashTable.get(2));
        assertEquals("three", hashTable.get(3));
        assertEquals("four", hashTable.get(4));
    }

    static void removeTest(HashTable<Integer, String> hashTable) {
        hashTable.put(1, "Hey");
        assertEquals(1, hashTable.size());
        hashTable.remove(1);
        assertEquals(0, hashTable.size());
        assertNull(hashTable.remove(12));
        hashTable.put(2, "two");
        assertNotNull(hashTable.remove(2));
        hashTable.put(1, "one");
        hashTable.put(12, "two");
        hashTable.remove(12);
        assertEquals("one", hashTable.get(1));
        hashTable.put(12, "two");
        hashTable.remove(1);
        assertEquals("two", hashTable.get(12));
    }

    static void putAllTest(HashTable<Integer, String> hashTable) {

        hashTable.put(1, "One");

        HashMap<Integer, String> mapToPut = new HashMap<>();
        mapToPut.put(1, "Two");
        mapToPut.put(2, "Three");

        hashTable.putAll(mapToPut);

        assertEquals(2, hashTable.size());
        assertEquals("Two", hashTable.get(1));
        assertEquals("Three", hashTable.get(2));
    }

    static void clearTest(HashTable<Integer, String> hashTable) {
        hashTable.put(1, "hey");
        hashTable.put(-15, "Dude");
        assertEquals(2, hashTable.size());
        hashTable.clear();
        assertTrue(hashTable.isEmpty());
        assertFalse(hashTable.containsKey(1));
    }

    static void keySetTest(HashTable<Integer, String> hashTable) {
        ArrayList<Integer> keys = new ArrayList<>();
        keys.add(1);
        keys.add(2);
        keys.add(3);

        hashTable.put(1, "one");
        hashTable.put(2, "two");
        hashTable.put(3, "three");

        for (Integer key : hashTable.keySet()) {
            keys.remove(key);
        }

        assertTrue(keys.isEmpty());
    }

    static void valuesTest(HashTable<Integer, String> hashTable) {
        ArrayList<String> values = new ArrayList<>();
        values.add("one");
        values.add("two");
        values.add("three");

        hashTable.put(1, "one");
        hashTable.put(2, "two");
        hashTable.put(3, "three");

        for (String value : hashTable.values()) {
            values.remove(value);
        }

        assertTrue(values.isEmpty());
    }

    static void entrySetTest(HashTable<Integer, String> hashTable) {
        ArrayList<Integer> keys = new ArrayList<>();
        keys.add(1);
        keys.add(2);
        keys.add(3);

        ArrayList<String> values = new ArrayList<>();
        values.add("one");
        values.add("two");
        values.add("three");

        hashTable.put(1, "one");
        hashTable.put(2, "two");
        hashTable.put(3, "three");

        for (Map.Entry<Integer, String> entry : hashTable.entrySet()) {
            keys.remove(entry.getKey());
            values.remove(entry.getValue());
        }

        assertTrue(keys.isEmpty());
        assertTrue(values.isEmpty());
    }

    static void getOrDefaultTest(HashTable<Integer, String> hashTable) {
        hashTable.put(1, "One");
        assertEquals("One", hashTable.getOrDefault(1, "Two"));
        assertEquals("Two", hashTable.getOrDefault(12, "Two"));
    }

    static void putIfAbsentTest(HashTable<Integer, String> hashTable) {
        assertNull(hashTable.putIfAbsent(1, "One"));
        assertNotNull(hashTable.putIfAbsent(1, "Two"));
        assertEquals("One", hashTable.get(1));
        assertEquals(1, hashTable.size());
    }

    static void removeIfValueTest(HashTable<Integer, String> hashTable) {
        hashTable.put(1, "One");
        hashTable.remove(1, "One");
        assertTrue(hashTable.isEmpty());
        hashTable.put(1, "One");
        hashTable.remove(1, "Two");
        assertFalse(hashTable.isEmpty());
    }

    static void replaceIfOldValueTest(HashTable<Integer, String> hashTable) {
        hashTable.put(1, "One");
        hashTable.replace(1, "One", "Two");
        assertEquals("Two", hashTable.get(1));
        hashTable.replace(1, "One", "Three");
        assertEquals("Two", hashTable.get(1));
    }

    static void replaceTest(HashTable<Integer, String> hashTable) {
        hashTable.put(1, "hey");
        hashTable.replace(1, "Dude");
        assertEquals("Dude", hashTable.get(1));
    }

}