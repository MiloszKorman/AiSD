package trees;

import java.util.function.BiFunction;

public class BSTDictionary<K extends Comparable<? super K>, V> {

    protected int nOfNodes = 0;

    protected Node<K, V> root;

    protected BiFunction<K, K, Integer> compareKeys = (key1, key2) -> key1.compareTo(key2);

    public void insert(K key, V value) {
        if (root == null) {
            root = new Node<>(key, value);
            nOfNodes++;
        } else {
            if (insertNode(root, new Node<>(key, value), compareKeys)) {
                nOfNodes++;
            }
        }
    }

    public void remove(K key) {
        if (root.key.equals(key) && root.left == null && root.right == null) {
            root = null;
            nOfNodes--;
        } else if (removeKey(root, key, compareKeys, root)) {
            nOfNodes--;
        }
    }

    public V find(K key) {
        Node<K, V> found = findNode(root, key, compareKeys);
        return (found != null) ? found.value : null;
    }

    public V getMin() {
        Node<K, V> min = findMinFrom(root);
        return (min != null) ? min.value : null;
    }

    public V getMax() {
        Node<K, V> max = findMaxFrom(root);
        return (max != null) ? max.value : null;
    }

    public int getNOfInternalNodes() {
        return (nOfNodes - findNumberOfNodesLeaves(root));
    }

    public int getHeight() {
        return (root == null) ? -1 : findNodesHeight(root);
    }

    public int getNOfNodes() {
        return nOfNodes;
    }

    public int getNOfLeaves() {
        return findNumberOfNodesLeaves(root);
    }

    public Node<K, V> getRoot() {
        return root;
    }

    protected static boolean insertNode(Node currentNode, Node wrapperToInsert, BiFunction compareKeys) {
        if ((int)compareKeys.apply(currentNode.key, wrapperToInsert.key) < 0) {
            if (currentNode.right == null) {
                currentNode.right = wrapperToInsert;
                wrapperToInsert.parent = currentNode;
                return true;
            } else {
                return insertNode(currentNode.right, wrapperToInsert, compareKeys);
            }
        } else if (0 < (int)compareKeys.apply(currentNode.key, wrapperToInsert.key)) {
            if (currentNode.left == null) {
                currentNode.left = wrapperToInsert;
                wrapperToInsert.parent = currentNode;
                return true;
            } else {
                return insertNode(currentNode.left, wrapperToInsert, compareKeys);
            }
        } else {
            currentNode.value = wrapperToInsert.value;
            return false;
        }
    }

    protected static boolean removeKey(Node currentNode, Object key, BiFunction compareKeys, Node root) {
        if ((int) compareKeys.apply(currentNode.key, key) < 0 && currentNode.right != null) { //go right
            return removeKey(currentNode.right, key, compareKeys, root);
        } else if (0 < (int) compareKeys.apply(currentNode.key, key) && currentNode.left != null) { // go left
            return removeKey(currentNode.left, key, compareKeys, root);
        } else if (currentNode.key.equals(key)) { //we found the dude
            Node substitute = findSuccessor(currentNode);
            if (substitute == null) {
                if (currentNode.parent.left == currentNode) {
                    currentNode.parent.left = null;
                } else {
                    currentNode.parent.right = null;
                }
                return true;
            } else {
                currentNode.key = substitute.key;
                currentNode.value = substitute.value;
                if (substitute.left == null && substitute.right == null) {
                    if (substitute.parent.left == substitute) {
                        substitute.parent.left = null;
                    } else {
                        substitute.parent.right = null;
                    }
                } else {
                    removeKey(substitute, substitute.key, compareKeys, root);
                }

                return true;
            }

        }

        return false;
    }

    protected static Node findNode(Node node, Object key, BiFunction compareKeys) {
        if (node == null || node.key.equals(key)) {
            return node;
        } else if ((int) compareKeys.apply(node.key, key) < 0) {
            return findNode(node.right, key, compareKeys);
        } else {
            return findNode(node.left, key, compareKeys);
        }
    }

    protected static Node findSuccessor(Node from) {
        Node biggerSuccessor = findMinFrom(from.right);
        return (biggerSuccessor == null) ? findMaxFrom(from.left) : biggerSuccessor;
    }

    protected static Node findMinFrom(Node from) {
        if (from == null || from.left == null) {
            return from;
        } else {
            return findMinFrom(from.left);
        }
    }

    protected static Node findMaxFrom(Node from) {
        if (from == null || from.right == null) {
            return from;
        } else {
            return findMaxFrom(from.right);
        }
    }

    protected static int findNodesHeight(Node node) {
        if (node.left == null && node.right == null) {
            return 0;
        } else {
            int leftHeight = 0;
            if (node.left != null) {
                leftHeight = 1 + findNodesHeight(node.left);
            }

            int rightHeight = 0;
            if (node.right != null) {
                rightHeight = 1 + findNodesHeight(node.right);
            }

            return (leftHeight < rightHeight) ? rightHeight : leftHeight;
        }
    }

    protected static int findNumberOfNodesLeaves(Node node) {

        if (node == null) {
            return 0;
        }

        if (node.left == null && node.right == null) { //this node is a leaf
            return 1;
        }

        return findNumberOfNodesLeaves(node.left) + findNumberOfNodesLeaves(node.right);
    }

    public static class Node<K, V> {

        K key;
        V value;
        Node<K, V> parent;
        Node<K, V> left;
        Node<K, V> right;

        Node() {
        }

        Node(K key, V value, Node parent) {
            this(key, value);
            this.parent = parent;
        }

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public Node<K, V> getParent() {
            return parent;
        }

        public Node<K, V> getLeft() {
            return left;
        }

        public Node<K, V> getRight() {
            return right;
        }

    }

}
