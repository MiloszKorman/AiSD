package trees;

public class RBTDictionary<K extends Comparable<? super K>, V> extends BSTDictionary<K, V> {

    @Override
    public void insert(K key, V value) {
        if (root == null) {
            root = new ColouredNode<>(key, value, ColouredNode.Colour.BLACK);
            nOfNodes++;
        } else {
            Node<K, V> possiblyAlreadyHere = findNode(root, key, compareKeys);
            if (possiblyAlreadyHere == null) {
                if (insertNode(root, new ColouredNode<>(key, value, ColouredNode.Colour.BLACK), compareKeys)) {
                    nOfNodes++;
                }
                fixAfterInsertion((ColouredNode) findNode(root, key, compareKeys), (ColouredNode) root);
                while (parentOf((ColouredNode<K, V>) root) != null) {
                    root = parentOf((ColouredNode<K, V>) root);
                }
                setColour((ColouredNode<K, V>) root, ColouredNode.Colour.BLACK);
            } else {
                possiblyAlreadyHere.value = value;
            }
        }
    }

    @Override
    public void remove(K key) {
        ColouredNode p = (ColouredNode) findNode(root, key, compareKeys);
        if (p != null) {
            nOfNodes--;
            if (leftOf(p) != null && rightOf(p) != null) {
                ColouredNode s = (ColouredNode) findSuccessor(p);
                p.key = s.key;
                p.value = s.value;
                p = s;
            }

            ColouredNode replacement = (ColouredNode) (p.left != null ? p.left : p.right);

            if (replacement != null) { //just swap replacement for deleted node
                replacement.parent = p.parent;
                if (p.parent == null) {
                    root = replacement;
                } else if (p == p.parent.left) {
                    p.parent.left = replacement;
                } else {
                    p.parent.right = replacement;
                }

                p.left = p.right = p.parent = null;
                if (p.colour == ColouredNode.Colour.BLACK) {
                    fixAfterDeletion(replacement, (ColouredNode) root);
                }
            } else if (parentOf(p) == null) { //we delete root, when there's only root in the tree
                root = null;
            } else {
                if (p.colour == ColouredNode.Colour.BLACK) {
                    fixAfterDeletion(p, (ColouredNode) root);
                }

                if (p.parent != null) {
                    if (p == p.parent.left) {
                        p.parent.left = null;
                    } else if (p == p.parent.right) {
                        p.parent.right = null;
                    }
                    p.parent = null;
                }
            }
        }

        while (root.parent != null) {
            root = root.parent;
        }
    }

    protected static void fixAfterInsertion(ColouredNode node, ColouredNode root) {
        if (node != null) {
            setColour(node, ColouredNode.Colour.RED);
            if (node != root && colourOf(parentOf(node)) == ColouredNode.Colour.RED) {
                ColouredNode father = parentOf(node);
                ColouredNode grandpa = parentOf(father);
                ColouredNode uncle;
                if (leftOf(grandpa) == father) {
                    uncle = rightOf(grandpa);
                } else {
                    uncle = leftOf(grandpa);
                }

                if (colourOf(uncle) == ColouredNode.Colour.RED) { //red uncle
                    setColour(father, ColouredNode.Colour.BLACK);
                    setColour(uncle, ColouredNode.Colour.BLACK);
                    setColour(grandpa, ColouredNode.Colour.RED);
                    fixAfterInsertion(grandpa, root);
                } else {                                        //black uncle
                    if (father == leftOf(grandpa)) {
                        if (node == leftOf(father)) { //Left Left Case
                            rotateRight(grandpa);
                            swapColours(father, grandpa);
                        } else if (node == rightOf(father)) { //Left Right Case
                            rotateLeft(father);
                            rotateRight(grandpa);
                            swapColours(node, grandpa);
                        }
                    } else if (father == rightOf(grandpa)) { //Right Right Case
                        if (node == rightOf(father)) {
                            rotateLeft(grandpa);
                            swapColours(father, grandpa);
                        } else if (node == leftOf(father)) { //Right Left Case
                            rotateRight(father);
                            rotateLeft(grandpa);
                            swapColours(node, grandpa);
                        }
                    }
                }
            }

        }
    }

    protected static void fixAfterDeletion(ColouredNode x, ColouredNode root) {
        while (x != root && colourOf(x) == ColouredNode.Colour.BLACK) {
            if (x == leftOf(parentOf(x))) {
                ColouredNode sibling = rightOf(parentOf(x));

                if (colourOf(sibling) == ColouredNode.Colour.RED) {
                    setColour(sibling, ColouredNode.Colour.BLACK);
                    setColour(parentOf(x), ColouredNode.Colour.RED);
                    rotateLeft(parentOf(x));
                    sibling = rightOf(parentOf(x));
                }

                if ((colourOf(leftOf(sibling)) == ColouredNode.Colour.BLACK || colourOf(leftOf(sibling)) == null)
                        &&
                        (colourOf(rightOf(sibling)) == ColouredNode.Colour.BLACK || colourOf(rightOf(sibling)) == null)) {
                    setColour(sibling, ColouredNode.Colour.RED);
                    x = parentOf(x);
                } else {
                    if (colourOf(rightOf(sibling)) == ColouredNode.Colour.BLACK || colourOf(rightOf(sibling)) == null) {
                        setColour(leftOf(sibling), ColouredNode.Colour.BLACK);
                        setColour(sibling, ColouredNode.Colour.RED);
                        rotateRight(sibling);
                        sibling = rightOf(parentOf(x));
                    }
                    setColour(sibling, colourOf(parentOf(x)));
                    setColour(parentOf(x), ColouredNode.Colour.BLACK);
                    setColour(rightOf(sibling), ColouredNode.Colour.BLACK);
                    rotateLeft(parentOf(x));
                    x = root;
                }
            } else {
                ColouredNode sibling = leftOf(parentOf(x));

                if (colourOf(sibling) == ColouredNode.Colour.RED) {
                    setColour(sibling, ColouredNode.Colour.BLACK);
                    setColour(parentOf(x), ColouredNode.Colour.RED);
                    rotateRight(parentOf(x));
                    sibling = leftOf(parentOf(x));
                }

                if ((colourOf(rightOf(sibling)) == ColouredNode.Colour.BLACK || colourOf(rightOf(sibling)) == null)
                        &&
                        (colourOf(leftOf(sibling)) == ColouredNode.Colour.BLACK || colourOf(leftOf(sibling)) == null)) {
                    setColour(sibling, ColouredNode.Colour.RED);
                    x = parentOf(x);
                } else {
                    if (colourOf(leftOf(sibling)) == ColouredNode.Colour.BLACK || colourOf(leftOf(sibling)) == null) {
                        setColour(rightOf(sibling), ColouredNode.Colour.BLACK);
                        setColour(sibling, ColouredNode.Colour.RED);
                        rotateLeft(sibling);
                        sibling = leftOf(parentOf(x));
                    }
                    setColour(sibling, colourOf(parentOf(x)));
                    setColour(parentOf(x), ColouredNode.Colour.BLACK);
                    setColour(leftOf(sibling), ColouredNode.Colour.BLACK);
                    rotateRight(parentOf(x));
                    x = root;
                }
            }
        }

        setColour(x, ColouredNode.Colour.BLACK);
    }

    protected static void swapColours(ColouredNode<?, ?> one, ColouredNode<?, ?> two) {
        if (one == null) {
            one = new ColouredNode<>(ColouredNode.Colour.BLACK);
        }
        if (two == null) {
            two = new ColouredNode<>(ColouredNode.Colour.BLACK);
        }

        ColouredNode.Colour colourHolder = colourOf(one);
        setColour(one, colourOf(two));
        setColour(two, colourHolder);
    }

    protected static void rotateRight(ColouredNode z) {
        ColouredNode y = leftOf(z);
        ColouredNode parentHolder = parentOf(z);
        ColouredNode subTreeHolder = rightOf(y);
        setParent(z, y);
        setParent(y, parentHolder);
        if (z == leftOf(parentHolder)) {
            setLeft(parentHolder, y);
        } else {
            setRight(parentHolder, y);
        }
        setRight(y, z);
        setLeft(z, subTreeHolder);
        setParent(subTreeHolder, z);
    }

    protected static void rotateLeft(ColouredNode z) {
        ColouredNode y = rightOf(z);
        ColouredNode parentHolder = parentOf(z);
        ColouredNode subTreeHolder = leftOf(y);
        setParent(z, y);
        setParent(y, parentHolder);
        if (z == leftOf(parentHolder)) {
            setLeft(parentHolder, y);
        } else {
            setRight(parentHolder, y);
        }
        setLeft(y, z);
        setRight(z, subTreeHolder);
        setParent(subTreeHolder, z);
    }

    protected static ColouredNode parentOf(ColouredNode node) {
        return (node == null) ? null : (ColouredNode) node.parent;
    }

    protected static ColouredNode leftOf(ColouredNode node) {
        return (node == null) ? null : (ColouredNode) node.left;
    }

    protected static ColouredNode rightOf(ColouredNode node) {
        return (node == null) ? null : (ColouredNode) node.right;
    }

    protected static ColouredNode.Colour colourOf(ColouredNode node) {
        if (node == null) {
            return null;
        } else {
            return node.colour;
        }
    }

    protected static void setParent(ColouredNode forWho, ColouredNode whom) {
        if (forWho != null) {
            forWho.parent = whom;
        }
    }

    protected static void setLeft(ColouredNode forWho, ColouredNode whom) {
        if (forWho != null) {
            forWho.left = whom;
        }
    }

    protected static void setRight(ColouredNode forWho, ColouredNode whom) {
        if (forWho != null) {
            forWho.right = whom;
        }
    }

    protected static void setColour(ColouredNode node, ColouredNode.Colour colour) {
        if (node != null) {
            node.colour = colour;
        }
    }

    public static class ColouredNode<K, V> extends Node<K, V> {

        Colour colour;

        ColouredNode(Colour colour) {
            this.colour = colour;
        }

        ColouredNode(K key, V value, Node parent, Colour colour) {
            super(key, value, parent);
            this.colour = colour;
        }

        ColouredNode(K key, V value, Node parent) {
            super(key, value, parent);
        }

        ColouredNode(K key, V value, Colour colour) {
            super(key, value);
            this.colour = colour;
        }

        ColouredNode(K key, V value) {
            super(key, value);
        }

        public Colour getColour() {
            return colour;
        }


        public enum Colour {
            RED, BLACK
        }

    }

}
