package trees.services;

import trees.BSTDictionary;
import trees.RBTDictionary;

public class ConsolePrintTree implements VisualizeTree {

    @Override
    public void visualize(BSTDictionary tree) {
        treePrint(tree.getRoot(), 0);
    }

    private static void treePrint(BSTDictionary.Node node, int indent) {
        if (node != null) {
            treePrint(node.getRight(), indent + 10);

            for (int i = indent + 2; 0 < i; i--) {
                System.out.print(" ");
            }
            System.out.println("/");

            for (int i = indent; 0 < i; i--) {
                System.out.print(" ");
            }
            if (node.getClass() == RBTDictionary.ColouredNode.class && ((RBTDictionary.ColouredNode<?, ?>) node).getColour() == RBTDictionary.ColouredNode.Colour.RED) {
                System.out.println("\u001B[31m" + node.getKey() + "\u001B[0m");
            } else {
                System.out.println(node.getKey());
            }

            for (int i = indent + 2; 0 < i; i--) {
                System.out.print(" ");
            }
            System.out.println("\\");

            treePrint(node.getLeft(), indent + 10);
        }
    }

}
