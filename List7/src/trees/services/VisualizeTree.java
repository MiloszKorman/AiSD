package trees.services;

import trees.BSTDictionary;

public interface VisualizeTree {

    void visualize(BSTDictionary tree);

}
