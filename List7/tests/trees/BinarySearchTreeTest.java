package trees;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTreeTest {

    @Test
    void insertTest() {
        BSTDictionary<Integer, String> binarySearchTree = new BSTDictionary<>();
        binarySearchTree.insert(1, "one");
        binarySearchTree.insert(2, "two");
        assertEquals("one", binarySearchTree.find(1));
        assertEquals("two", binarySearchTree.find(2));
        assertEquals(1, binarySearchTree.getHeight());
        assertEquals(2, binarySearchTree.getNOfNodes());
        binarySearchTree.insert(2, "three");
        assertEquals("three", binarySearchTree.find(2));
    }

    @Test
    void removeTest() {
        BSTDictionary<Integer, String> binarySearchTree = new BSTDictionary<>();
        binarySearchTree.insert(1, "one");
        binarySearchTree.insert(2, "two");
        assertEquals("one", binarySearchTree.find(1));
        assertEquals("two", binarySearchTree.find(2));
        assertEquals(1, binarySearchTree.getHeight());
        assertEquals(2, binarySearchTree.getNOfNodes());
        binarySearchTree.remove(2);
        assertNull(binarySearchTree.find(2));
        assertEquals(0, binarySearchTree.getHeight());
        assertEquals(1, binarySearchTree.getNOfNodes());

        binarySearchTree = new BSTDictionary<>();
        binarySearchTree.insert(10, "ten");
        binarySearchTree.insert(8, "eight");
        binarySearchTree.insert(9, "nine");
        binarySearchTree.insert(6, "six");
        binarySearchTree.insert(7, "seven");
        binarySearchTree.insert(5, "five");
        binarySearchTree.insert(11, "eleven");
        binarySearchTree.insert(12, "twelve");
        binarySearchTree.insert(14, "fourteen");
        binarySearchTree.insert(13, "thirteen");
        binarySearchTree.insert(15, "fifteen");

        binarySearchTree.remove(14);
        assertNull(binarySearchTree.find(14));
        assertEquals(10, binarySearchTree.getNOfNodes());
        assertEquals(4, binarySearchTree.getNOfLeaves());
        binarySearchTree.remove(8);
        assertNull(binarySearchTree.find(8));
        assertEquals(9, binarySearchTree.getNOfNodes());
        assertEquals(3, binarySearchTree.getNOfLeaves());
        binarySearchTree.remove(15);
        assertNull(binarySearchTree.find(15));
        assertEquals(8, binarySearchTree.getNOfNodes());
        assertEquals(3, binarySearchTree.getNOfLeaves());
        binarySearchTree.remove(10);
        assertNull(binarySearchTree.find(10));
        assertEquals(7, binarySearchTree.getNOfNodes());
        assertEquals(3, binarySearchTree.getNOfLeaves());
    }

    @Test
    void getMinAndMaxTest() {
        getMinAndMax(new BSTDictionary<>());

    }

    static void getMinAndMax(BSTDictionary<Integer, String> tree) {
        tree.insert(3, "three");
        tree.insert(1, "one");
        tree.insert(2, "two");
        tree.insert(4, "four");
        tree.insert(5, "five");
        assertEquals("one", tree.getMin());
        assertEquals("five", tree.getMax());
    }

    @Test
    void getNOfInternalNodesTest() {
        BSTDictionary<Integer, String> binarySearchTree = new BSTDictionary<>();
        binarySearchTree.insert(10, "ten");
        binarySearchTree.insert(8, "eight");
        binarySearchTree.insert(9, "nine");
        binarySearchTree.insert(6, "six");
        binarySearchTree.insert(7, "seven");
        binarySearchTree.insert(5, "five");
        binarySearchTree.insert(11, "eleven");
        binarySearchTree.insert(12, "twelve");
        binarySearchTree.insert(14, "fourteen");
        binarySearchTree.insert(13, "thirteen");
        binarySearchTree.insert(15, "fifteen");

        assertEquals(6, binarySearchTree.getNOfInternalNodes());
    }

    @Test
    void getNOfLeavesTest() {
        BSTDictionary<Integer, String> binarySearchTree = new BSTDictionary<>();
        binarySearchTree.insert(10, "ten");
        binarySearchTree.insert(8, "eight");
        binarySearchTree.insert(9, "nine");
        binarySearchTree.insert(6, "six");
        binarySearchTree.insert(7, "seven");
        binarySearchTree.insert(5, "five");
        binarySearchTree.insert(11, "eleven");
        binarySearchTree.insert(12, "twelve");
        binarySearchTree.insert(14, "fourteen");
        binarySearchTree.insert(13, "thirteen");
        binarySearchTree.insert(15, "fifteen");

        assertEquals(5, binarySearchTree.getNOfLeaves());
    }

    @Test
    void getHeightTest() {
        BSTDictionary<Integer, String> binarySearchTree = new BSTDictionary<>();
        binarySearchTree.insert(15, "fifteen");
        binarySearchTree.insert(10, "ten");
        binarySearchTree.insert(20, "twenty");
        binarySearchTree.insert(17, "seventeen");
        binarySearchTree.insert(19, "nineteen");
        binarySearchTree.insert(18, "eighteen");
        assertEquals(4, binarySearchTree.getHeight());
    }


}