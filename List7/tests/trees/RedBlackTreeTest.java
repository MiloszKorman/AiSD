package trees;

import org.junit.jupiter.api.Test;
import trees.services.ConsolePrintTree;

import static org.junit.jupiter.api.Assertions.*;

class RedBlackTreeTest {

    @Test
    void basicInsertTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(5, "five");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(100, "hundred");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(42, "forty two");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(49, "forty nine");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(35, "thirty five");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(36, "thirty six");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(62, "sixty two");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(3, "three");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(38, "thirty eight");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(43, "forty three");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(10, "ten");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(77, "seventy seven");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(1, "one");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(41, "forty one");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(50, "fifty");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(11, "eleven");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(25, "twenty five");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(59, "fifty nine");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(98, "ninety eight");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(44, "forty four");
        checkWhetherBalanced(redBlackTree.root, 0);
    }

    @Test
    void duplicateInsertionTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(2, "two");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(1, "one");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(3, "three");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(1, "uno");
        assertEquals("uno", redBlackTree.find(1));
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(3, "tres");
        assertEquals("tres", redBlackTree.find(3));
        checkWhetherBalanced(redBlackTree.root, 0);
    }

    @Test
    void insertRedUncleTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(10, "ten");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(5, "five");
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.insert(15, "fifteen");
        checkWhetherBalanced(redBlackTree.root, 0);

        assertEquals("ten", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("five", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("fifteen", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);


        redBlackTree.insert(2, "two");
        checkWhetherBalanced(redBlackTree.root, 0);

        assertEquals("ten", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("five", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("fifteen", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
        assertEquals("two", redBlackTree.root.left.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.left.left).colour);
    }

    @Test
    void insertLeftLeftCaseTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(10, "ten");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(5, "five", redBlackTree.root, RBTDictionary.ColouredNode.Colour.RED);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(15, "fifteen", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);

        assertEquals("ten", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("five", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("fifteen", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);

        redBlackTree.insert(2, "two");

        assertEquals("five", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("two", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("ten", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
        assertEquals("fifteen", redBlackTree.root.right.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right.right).colour);
    }

    @Test
    void insertLeftRightCaseTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(10, "ten");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(5, "five", redBlackTree.root, RBTDictionary.ColouredNode.Colour.RED);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(15, "fifteen", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);

        assertEquals("ten", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("five", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("fifteen", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);

        redBlackTree.insert(7, "seven");

        assertEquals("seven", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("five", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("ten", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
        assertEquals("fifteen", redBlackTree.root.right.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right.right).colour);
    }

    @Test
    void insertRightRightCaseTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(10, "ten");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(5, "five", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(15, "fifteen", redBlackTree.root, RBTDictionary.ColouredNode.Colour.RED);

        assertEquals("ten", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("five", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("fifteen", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);

        redBlackTree.insert(17, "seventeen");

        assertEquals("fifteen", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("ten", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("seventeen", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
        assertEquals("five", redBlackTree.root.left.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left.left).colour);
    }

    @Test
    void insertRightLeftCaseTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(10, "ten");

        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(5, "five", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(15, "fifteen", redBlackTree.root, RBTDictionary.ColouredNode.Colour.RED);

        assertEquals("ten", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("five", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("fifteen", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);

        redBlackTree.insert(13, "thirteen");

        assertEquals("thirteen", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("ten", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("fifteen", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
        assertEquals("five", redBlackTree.root.left.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left.left).colour);
    }

    @Test
    void basicRemoveTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(5, "five");
        redBlackTree.insert(100, "hundred");
        redBlackTree.insert(42, "forty two");
        redBlackTree.insert(49, "forty nine");
        redBlackTree.insert(35, "thirty five");
        redBlackTree.insert(36, "thirty six");
        redBlackTree.insert(62, "sixty two");
        redBlackTree.insert(3, "three");
        redBlackTree.insert(38, "thirty eight");
        redBlackTree.insert(43, "forty three");
        redBlackTree.insert(10, "ten");
        redBlackTree.insert(77, "seventy seven");
        redBlackTree.insert(1, "one");
        redBlackTree.insert(41, "forty one");
        redBlackTree.insert(50, "fifty");
        redBlackTree.insert(11, "eleven");
        redBlackTree.insert(25, "twenty five");
        redBlackTree.insert(59, "fifty nine");
        redBlackTree.insert(98, "ninety eight");
        redBlackTree.insert(44, "forty four");

        redBlackTree.remove(42);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(50);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(5);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(62);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(11);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(77);
        checkWhetherBalanced(redBlackTree.root, 0);

        redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(5, "five");
        redBlackTree.insert(100, "hundred");
        redBlackTree.insert(42, "forty two");
        redBlackTree.insert(49, "forty nine");
        redBlackTree.insert(35, "thirty five");
        redBlackTree.insert(36, "thirty six");
        redBlackTree.insert(62, "sixty two");
        redBlackTree.insert(3, "three");
        redBlackTree.insert(38, "thirty eight");
        redBlackTree.insert(43, "forty three");
        redBlackTree.insert(10, "ten");
        redBlackTree.insert(77, "seventy seven");
        redBlackTree.insert(1, "one");
        redBlackTree.insert(41, "forty one");
        redBlackTree.insert(50, "fifty");
        redBlackTree.insert(11, "eleven");
        redBlackTree.insert(25, "twenty five");
        redBlackTree.insert(59, "fifty nine");
        redBlackTree.insert(98, "ninety eight");
        redBlackTree.insert(44, "forty four");
        checkWhetherBalanced(redBlackTree.root, 0);

        redBlackTree.remove(62);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(42);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(44);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(59);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(50);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(35);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(25);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(11);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(10);
        checkWhetherBalanced(redBlackTree.root, 0);
        redBlackTree.remove(49);
        checkWhetherBalanced(redBlackTree.root, 0);

        assertEquals("five", redBlackTree.find(5));
        assertEquals("hundred", redBlackTree.find(100));
        assertEquals("thirty six", redBlackTree.find(36));
        assertEquals("three", redBlackTree.find(3));
        assertEquals("thirty eight", redBlackTree.find(38));
        assertEquals("forty three", redBlackTree.find(43));
        assertEquals("seventy seven", redBlackTree.find(77));
        assertEquals("one", redBlackTree.find(1));
        assertEquals("forty one", redBlackTree.find(41));
        assertEquals("ninety eight", redBlackTree.find(98));
        assertNull(redBlackTree.find(62));
        assertNull(redBlackTree.find(42));
        assertNull(redBlackTree.find(44));
        assertNull(redBlackTree.find(59));
        assertNull(redBlackTree.find(50));
        assertNull(redBlackTree.find(35));
        assertNull(redBlackTree.find(25));
        assertNull(redBlackTree.find(11));
        assertNull(redBlackTree.find(10));
        assertNull(redBlackTree.find(49));
    }

    @Test
    void deletionDeletedOrSuccessorRedTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(30, "thirty");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(20, "twenty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(40, "forty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.left.left = new RBTDictionary.ColouredNode<>(10, "ten", redBlackTree.root.left, RBTDictionary.ColouredNode.Colour.RED);
        redBlackTree.remove(20);

        assertEquals("thirty", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("ten", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("forty", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
    }

    @Test
    void deletionSiblingBlackChildrenRedLLTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(30, "thirty");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(20, "twenty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(40, "forty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.left.left = new RBTDictionary.ColouredNode<>(10, "ten", redBlackTree.root.left, RBTDictionary.ColouredNode.Colour.RED);
        redBlackTree.root.left.right = new RBTDictionary.ColouredNode<>(25, "twenty five", redBlackTree.root.left, RBTDictionary.ColouredNode.Colour.RED);
        redBlackTree.remove(40);

        assertEquals("twenty", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("ten", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("thirty", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
        assertEquals("twenty five", redBlackTree.root.right.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.right.left).colour);
    }

    @Test
    void deletionSiblingBlackChildrenRedLRTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(30, "thirty");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(20, "twenty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(40, "forty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.left.right = new RBTDictionary.ColouredNode<>(25, "twenty five", redBlackTree.root.left, RBTDictionary.ColouredNode.Colour.RED);

        redBlackTree.remove(40);

        assertEquals("twenty five", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("twenty", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("thirty", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
    }

    @Test
    void deletionSiblingBlackChildrenRedRRTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(30, "thirty");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(20, "twenty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(40, "forty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right.left = new RBTDictionary.ColouredNode<>(35, "thirty five", redBlackTree.root.right, RBTDictionary.ColouredNode.Colour.RED);
        redBlackTree.root.right.right = new RBTDictionary.ColouredNode<>(50, "fifty", redBlackTree.root.right, RBTDictionary.ColouredNode.Colour.RED);
        redBlackTree.remove(20);

        assertEquals("forty", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("thirty", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("fifty", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
        assertEquals("thirty five", redBlackTree.root.left.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.left.right).colour);
    }

    @Test
    void deletionSiblingBlackChildrenRedRLTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(30, "thirty");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(20, "twenty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(40, "forty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right.left = new RBTDictionary.ColouredNode<>(35, "thirty five", redBlackTree.root.right, RBTDictionary.ColouredNode.Colour.RED);
        redBlackTree.remove(20);

        assertEquals("thirty five", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("thirty", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.left).colour);
        assertEquals("forty", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
    }

    @Test
    void deletionSiblingAndBothChildrenBlackTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(20, "twenty");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(10, "ten", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(30, "thirty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.remove(10);

        assertEquals("twenty", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode)redBlackTree.root).colour);
        assertEquals("thirty", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode)redBlackTree.root.right).colour);
    }

    @Test
    void deletionSiblingRedTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        redBlackTree.insert(20, "twenty");
        redBlackTree.root.left = new RBTDictionary.ColouredNode<>(10, "ten", redBlackTree.root, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right = new RBTDictionary.ColouredNode<>(30, "thirty", redBlackTree.root, RBTDictionary.ColouredNode.Colour.RED);
        redBlackTree.root.right.left = new RBTDictionary.ColouredNode<>(25, "twenty five", redBlackTree.root.right, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.root.right.right = new RBTDictionary.ColouredNode<>(35, "thirty five", redBlackTree.root.right, RBTDictionary.ColouredNode.Colour.BLACK);
        redBlackTree.remove(10);

        assertEquals("thirty", redBlackTree.root.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode) redBlackTree.root).colour);
        assertEquals("twenty", redBlackTree.root.left.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode) redBlackTree.root.left).colour);
        assertEquals("thirty five", redBlackTree.root.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.BLACK, ((RBTDictionary.ColouredNode) redBlackTree.root.right).colour);
        assertEquals("twenty five", redBlackTree.root.left.right.value);
        assertEquals(RBTDictionary.ColouredNode.Colour.RED, ((RBTDictionary.ColouredNode) redBlackTree.root.left.right).colour);
    }

    @Test
    void getMinAndMaxTest() {
        BinarySearchTreeTest.getMinAndMax(new RBTDictionary<>());
    }

    @Test
    void getNOfLeafs() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        assertEquals(0, redBlackTree.getNOfLeaves());
        redBlackTree.insert(10, "ten");
        assertEquals(1, redBlackTree.getNOfLeaves());
        redBlackTree.insert(5, "five");
        redBlackTree.insert(15, "fifteen");
        assertEquals(2, redBlackTree.getNOfLeaves());
        redBlackTree.insert(2, "two");
        redBlackTree.insert(18, "eighteen");
        redBlackTree.insert(1, "one");
        redBlackTree.insert(19, "nineteen");
        redBlackTree.insert(3, "three");
        redBlackTree.insert(17, "seventeen");
        assertEquals(4, redBlackTree.getNOfLeaves());
    }

    @Test
    void getHeightTest() {
        RBTDictionary<Integer, String> redBlackTree = new RBTDictionary<>();
        assertEquals(-1, redBlackTree.getHeight());
        redBlackTree.insert(10, "ten");
        assertEquals(0, redBlackTree.getHeight());
        redBlackTree.insert(5, "five");
        redBlackTree.insert(15, "fifteen");
        redBlackTree.insert(20, "twenty");
        assertEquals(2, redBlackTree.getHeight());
    }

    static int checkWhetherBalanced(BSTDictionary.Node<?, ?> node, int length) {

        int leftLength = 0;
        int rightLength = 0;

        if (node != null) {
            leftLength = checkWhetherBalanced(node.left, length);
            rightLength = checkWhetherBalanced(node.right, length);
            assertEquals(leftLength, rightLength);
        }

        if (node == null || ((RBTDictionary.ColouredNode)node).colour == RBTDictionary.ColouredNode.Colour.BLACK) {
            length++;
        }

        return length + rightLength;
    }

    @Test
    void testeros() {
        RBTDictionary<Integer, Integer> redBlackTree = new RBTDictionary<>();

        for (int i = 0; i < 2000; i++) {
            int random = (int) (Math.random() * 1000) + 1;
            redBlackTree.insert(random, random);
            checkWhetherBalanced(redBlackTree.root, 0);
        }

        for (int i = 0; i < 1000; i++) {
            int random = (int) (Math.random() * 1000) + 1;
            redBlackTree.remove(random);
            checkWhetherBalanced(redBlackTree.root, 0);
        }

    }

    @Test
    void addOneToNine() {
        RBTDictionary<Integer, Integer> redBlackTree = new RBTDictionary<>();

        for (int i = 1; i < 10; i++) {
            redBlackTree.insert(i, i);
        }

        checkWhetherBalanced(redBlackTree.root, 0);

        redBlackTree.remove(4);

        (new ConsolePrintTree()).visualize(redBlackTree);

    }

}
