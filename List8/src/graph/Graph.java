package graph;

import java.util.List;

public abstract class Graph {

    int size;

    public Graph(int size) {

        if (size < 2) {
            throw new IllegalArgumentException("Size must be greater than 1");
        }

        this.size = size;
    }

    public Graph() {
        this(2);
    }

    /**
     * Adds new empty (no links) element to the graph
     */
    public abstract void addNewElement();

    /**
     * Deletes element with provided index and all its links, others indexes are shifted
     *
     * @param iToRemove index of element to remove from graph
     */
    public abstract void deleteElement(int iToRemove);

    /**
     * Adds link with provided weight between elements with provided indexes
     *
     * @param elem1  index of the first element
     * @param elem2  index of the second element
     * @param weight weight of connection between them
     */
    public abstract void addLink(int elem1, int elem2, int weight);

    /**
     * Adds link with weight od 1 between elements with provided indexes
     *
     * @param elem1 index of the first element
     * @param elem2 index of the second element
     */
    public void addLink(int elem1, int elem2) {
        addLink(elem1, elem2, 1);
    }

    /**
     * Removes link between two elements with provided indexes
     *
     * @param elem1 index of the first element
     * @param elem2 index of the second element
     */
    public abstract void removeLink(int elem1, int elem2);

    /**
     * Changes links weight between elements with provided indexes
     *
     * @param elem1  index of the first element
     * @param elem2  index of the second element
     * @param weight new weight of their connection
     */
    public abstract void changeLinksWeight(int elem1, int elem2, int weight);

    /**
     * Returns all links present in graph between its elements
     *
     * @return list of links between graphs elements
     */
    public abstract List<Link> getAllLinks();

    /**
     * Returns number of elements present in the graph
     *
     * @return graphs size
     */
    public int size() {
        return size;
    }

    public static class Link implements Comparable {

        public int firstNode;
        public int secondNode;
        public int weight;

        public Link(int firstNode, int secondNode, int weight) {
            this.firstNode = firstNode;
            this.secondNode = secondNode;
            this.weight = weight;
        }

        @Override
        public boolean equals(Object obj) {
            if (firstNode == ((Link) obj).firstNode && secondNode == ((Link) obj).secondNode) {
                return true;
            }
            return false;
        }

        @Override
        public int compareTo(Object o) {
            if (this.firstNode != ((Link) o).firstNode) {
                return Integer.compare(this.firstNode, ((Link) o).firstNode);
            } else {
                return Integer.compare(this.secondNode, ((Link) o).secondNode);
            }
        }
    }
}
