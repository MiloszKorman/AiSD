package graph;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ListGraph extends Graph {

    private LinkedList<Link> adjacencyList[];

    @SuppressWarnings("unchecked")
    public ListGraph(int size) {
        super(size);
        adjacencyList = new LinkedList[size];
        for (int i = 0; i < size; i++) {
            adjacencyList[i] = new LinkedList<>();
        }
    }

    @SuppressWarnings("unchecked")
    public ListGraph() {
        adjacencyList = new LinkedList[size];
        for (int i = 0; i < size; i++) {
            adjacencyList[i] = new LinkedList<>();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void addNewElement() {
        size++;
        LinkedList<Link> newList[] = new LinkedList[size];
        for (int elem = 0; elem < adjacencyList.length; elem++) {
            newList[elem] = adjacencyList[elem];
        }
        newList[newList.length - 1] = new LinkedList<>();
        adjacencyList = newList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteElement(int iToRemove) {

        if (iToRemove < 0 || adjacencyList.length <= iToRemove) {
            throw new IllegalArgumentException("number provided not in range of accessible numbers");
        }

        size--;
        LinkedList<Link> newList[] = new LinkedList[size];
        for (int i = 0; i < newList.length; i++) {
            newList[i] = new LinkedList<>();
        }

        for (int elem = 0; elem < iToRemove; elem++) {
            for (Link link : adjacencyList[elem]) {
                if (link.secondNode != iToRemove) { //skip the one to remove
                    if (iToRemove < link.secondNode) { //indexes of all elements after the one to remove decrease
                        link.secondNode--;
                    }
                    newList[elem].add(link);
                }
            }
        }
        for (int i = iToRemove + 1; i < adjacencyList.length; i++) {
            for (Link link : adjacencyList[i]) {
                if (link.secondNode != iToRemove) { //skip the one to remove
                    if (iToRemove < link.secondNode) { //indexes of all elements after the one to remove decrease
                        link.secondNode--;
                    }
                    link.firstNode--; //indexes of all elements after the one to remove decrease
                    newList[i - 1].add(link);
                }
            }
        }
        adjacencyList = newList;
    }

    @Override
    public void addLink(int elem1, int elem2, int weight) {

        if (elem1 < 0 || adjacencyList.length <= elem1) {
            throw new IllegalArgumentException("first node not in range of accessible numbers");
        }
        if (elem2 < 0 || adjacencyList.length <= elem2) {
            throw new IllegalArgumentException("second node not in range of accessible numbers");
        }
        if (weight < 1) {
            throw new IllegalArgumentException("weight must be a positive number");
        }

        if (elem2 < elem1) {
            int holder = elem2;
            elem2 = elem1;
            elem1 = holder;
        }

        Link link = new Link(elem1, elem2, weight);
        if (!adjacencyList[elem1].contains(link)) {
            adjacencyList[elem1].add(link);
        }
    }

    @Override
    public void removeLink(int elem1, int elem2) {

        if (elem1 < 0 || adjacencyList.length <= elem1) {
            throw new IllegalArgumentException("first node not in range of accessible numbers");
        }
        if (elem2 < 0 || adjacencyList.length <= elem2) {
            throw new IllegalArgumentException("second node not in range of accessible numbers");
        }

        if (elem2 < elem1) {
            int holder = elem2;
            elem2 = elem1;
            elem1 = holder;
        }

        adjacencyList[elem1].remove(new Link(elem1, elem2, 1));
    }

    @Override
    public void changeLinksWeight(int elem1, int elem2, int weight) {

        if (elem1 < 0 || adjacencyList.length <= elem1) {
            throw new IllegalArgumentException("first node not in range of accessible numbers");
        }
        if (elem2 < 0 || adjacencyList.length <= elem2) {
            throw new IllegalArgumentException("second node not in range of accessible numbers");
        }
        if (weight < 1) {
            throw new IllegalArgumentException("weight must be a positive number");
        }

        if (elem2 < elem1) {
            int holder = elem2;
            elem2 = elem1;
            elem1 = holder;
        }

        Link link = new Link(elem1, elem2, weight);
        if (adjacencyList[elem1].contains(link)) {
            adjacencyList[elem1].remove(link);
            adjacencyList[elem1].add(link);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Link> getAllLinks() {
        List<Link> toReturn = new LinkedList<>();
        for (LinkedList<Link> listOfLinks : adjacencyList) {
            toReturn.addAll(listOfLinks);
        }
        Collections.sort(toReturn);

        return toReturn;
    }

}
