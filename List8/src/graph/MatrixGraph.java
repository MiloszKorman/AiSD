package graph;

import java.util.LinkedList;
import java.util.List;

public class MatrixGraph extends Graph {

    private int adjacencyMatrix[][];

    public MatrixGraph(int size) {
        super(size);
        this.adjacencyMatrix = new int[size][size];
    }

    public MatrixGraph() {
        super();
        this.adjacencyMatrix = new int[size][size];
    }

    @Override
    public void addNewElement() {
        size++;
        int newMatrix[][] = new int[size][size];
        for (int row = 0; row < adjacencyMatrix.length; row++) {
            for (int val = 0; val < adjacencyMatrix[row].length; val++) {
                newMatrix[row][val] = adjacencyMatrix[row][val];
            }
        }
        adjacencyMatrix = newMatrix;
    }

    @Override
    public void deleteElement(int iToRemove) {

        if (iToRemove < 0 || adjacencyMatrix.length <= iToRemove) {
            throw new IllegalArgumentException("iToRemove provided not in range of accessible numbers");
        }

        size--;

        int newMatrix[][] = new int[size][size];

        /*Matrix is divided into 4 sectors, we need to skip row and column with index of "iToRemove"
        0 or all of them can be empty, in which case loops do not execute*/

        for (int row = 0; row < iToRemove; row++) { //copy upper left area
            for (int val = 0; val < iToRemove; val++) {
                newMatrix[row][val] = adjacencyMatrix[row][val];
            }
        }
        for (int row = iToRemove + 1; row < adjacencyMatrix.length; row++) { //copy bottom left area
            for (int val = 0; val < iToRemove; val++) {
                newMatrix[row - 1][val] = adjacencyMatrix[row][val];
            }
        }
        for (int row = 0; row < iToRemove; row++) { //copy upper right area
            for (int val = iToRemove + 1; val < adjacencyMatrix.length; val++) {
                newMatrix[row][val - 1] = adjacencyMatrix[row][val];
            }
        }
        for (int row = iToRemove + 1; row < adjacencyMatrix.length; row++) { //copy bottom right area
            for (int val = iToRemove + 1; val < adjacencyMatrix.length; val++) {
                newMatrix[row - 1][val - 1] = adjacencyMatrix[row][val];
            }
        }
        adjacencyMatrix = newMatrix;
    }

    @Override
    public void addLink(int elem1, int elem2, int weight) {

        if (elem1 < 0 || adjacencyMatrix.length <= elem1) {
            throw new IllegalArgumentException("first node not in range of accessible numbers");
        }
        if (elem2 < 0 || adjacencyMatrix.length <= elem2) {
            throw new IllegalArgumentException("second node not in range of accessible numbers");
        }
        if (weight < 1) {
            throw new IllegalArgumentException("weight must be a positive number");
        }

        if (adjacencyMatrix[elem1][elem2] == 0 && adjacencyMatrix[elem2][elem1] == 0) {
            adjacencyMatrix[elem1][elem2] = adjacencyMatrix[elem2][elem1] = weight;
        }
    }

    @Override
    public void removeLink(int elem1, int elem2) {

        if (elem1 < 0 || adjacencyMatrix.length <= elem1) {
            throw new IllegalArgumentException("first node not in range of accessible numbers");
        }
        if (elem2 < 0 || adjacencyMatrix.length <= elem2) {
            throw new IllegalArgumentException("second node not in range of accessible numbers");
        }

        adjacencyMatrix[elem1][elem2] = adjacencyMatrix[elem2][elem1] = 0;
    }

    @Override
    public void changeLinksWeight(int elem1, int elem2, int weight) {

        if (elem1 < 0 || adjacencyMatrix.length <= elem1) {
            throw new IllegalArgumentException("first node not in range of accessible numbers");
        }
        if (elem2 < 0 || adjacencyMatrix.length <= elem2) {
            throw new IllegalArgumentException("second node not in range of accessible numbers");
        }
        if (weight < 1) {
            throw new IllegalArgumentException("weight must be a positive number");
        }

        if (0 < adjacencyMatrix[elem1][elem2] && 0 < adjacencyMatrix[elem2][elem1]) {
            adjacencyMatrix[elem1][elem2] = adjacencyMatrix[elem2][elem1] = weight;
        }
    }

    @Override
    public List<Link> getAllLinks() {
        List<Link> toReturn = new LinkedList<>();

        for (int row = 0; row < size; row++) {
            for (int val = row; val < size; val++) {
                if (0 < adjacencyMatrix[row][val]) {
                    toReturn.add(new Link(row, val, adjacencyMatrix[row][val]));
                }
            }
        }

        return toReturn;
    }

}
