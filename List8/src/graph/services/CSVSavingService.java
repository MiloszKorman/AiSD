package graph.services;

import graph.Graph;
import graph.Graph.Link;
import graph.MatrixGraph;

import java.io.*;

public class CSVSavingService implements GraphStorageService {

    @Override
    public Graph importGraph(String fileName) {

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName + ".csv"))) {

            MatrixGraph graph = new MatrixGraph(Integer.parseInt(reader.readLine()));

            String line = reader.readLine();
            while (line != null && !line.isEmpty()) {
                String[] info = line.split(",");
                graph.addLink(Integer.parseInt(info[0]), Integer.parseInt(info[1]), Integer.parseInt(info[2]));
                line = reader.readLine();
            }

            reader.close();
            return graph;

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public void exportGraph(String fileName, Graph graph) {

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName + ".csv"))) {

            StringBuilder toWrite = new StringBuilder();

            toWrite.append(graph.size());
            for (Link link : graph.getAllLinks()) {
                toWrite.append("\n");
                toWrite.append(link.firstNode + ",");
                toWrite.append(link.secondNode + ",");
                toWrite.append(link.weight);
            }

            writer.write(toWrite.toString());

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
