package graph.services;

import graph.Graph;

public interface GraphStorageService {

    /**
     * Opens file by filename provided and reconstructs graphs structure from it
     *
     * @param fileName name by which the file containing graphs structure is sought
     * @return completely reconstructed graph
     */
    public Graph importGraph(String fileName);

    /**
     * Writes graphs structure to a save file
     *
     * @param fileName name under which file is stored
     * @param graph    graph which is ought to be stored
     */
    public void exportGraph(String fileName, Graph graph);

}
