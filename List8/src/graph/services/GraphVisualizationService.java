package graph.services;

import graph.Graph;
import graph.Graph.Link;

import java.util.List;

public interface GraphVisualizationService {

    /**
     * Visually presents graphs structure
     *
     * @param graph which is ought to be visualized
     */
    public void visualize(Graph graph);

    /**
     * Visually presents graphs structure with highlighted provided path
     *
     * @param graph which is ought to be visualized
     * @param path  path between two elements, is highlighted
     */
    public void visualizeWithShortestPath(Graph graph, List<Link> path);

}
