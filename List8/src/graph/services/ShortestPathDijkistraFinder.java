package graph.services;

import graph.Graph;
import graph.Graph.Link;

import java.util.*;

public class ShortestPathDijkistraFinder {

    /**
     * Finds shortest path in provided graph between elements with provided indexes
     *
     * @param graph in which shortest path is sought
     * @param start index of element from which path begins
     * @param end   index of element on which path ends
     * @return list with links of shortest path between start and end
     */
    public static List<Link> findShortestPath(Graph graph, int start, int end) {

        if (start < 0 || graph.size() <= start) {
            throw new IllegalArgumentException("number provided not in range of accessible numbers");
        }
        if (end < 0 || graph.size() <= end) {
            throw new IllegalArgumentException("number provided not in range of accessible numbers");
        }

        int array[][] = new int[graph.size()][2];

        for (int i = 0; i < graph.size(); i++) { //set initially all elements d as INF and p as -1
            array[i][0] = Integer.MAX_VALUE;
            array[i][1] = -1;
        }
        array[start][0] = 0; //set starts d as 0

        ArrayList<Integer> Q = new ArrayList<>();
        ArrayList<Integer> S = new ArrayList<>();
        for (int i = 0; i < graph.size(); i++) {
            Q.add(i);
        }

        for (int a = 0; a < graph.size(); a++) {

            int min = Integer.MAX_VALUE;
            int minI = Integer.MAX_VALUE;
            for (int i = 0; i < array.length; i++) {
                if (array[i][0] < min && Q.contains(i)) { //find closest available in Q
                    min = array[i][0];
                    minI = i;
                }
            }

            if (minI < Integer.MAX_VALUE) { //if found, remove from Q, add to S
                Q.remove(new Integer(minI));
                S.add(minI);
            }

            int current = minI;

            ArrayList<Link> neighbours = new ArrayList<>();
            for (Link link : graph.getAllLinks()) { //find all currents links
                if (link.firstNode == current || link.secondNode == current) {
                    neighbours.add(link);
                }
            }

            for (Link link : neighbours) { //for each currents link set d to them and p as current element
                if (link.firstNode == current) {
                    if (Q.contains(link.secondNode)) {
                        if (array[current][0] + link.weight < array[link.secondNode][0]) {
                            array[link.secondNode][0] = array[current][0] + link.weight;
                            array[link.secondNode][1] = current;
                        }
                    }
                } else {
                    if (Q.contains(link.firstNode)) {
                        if (array[current][0] + link.weight < array[link.firstNode][0]) {
                            array[link.firstNode][0] = array[current][0] + link.weight;
                            array[link.firstNode][1] = current;
                        }
                    }
                }
            }

        }

        List<Link> path = new ArrayList<>();
        int current = end;
        while (current != start && current != -1) { //add links for shortest path to path

            for (Link link : graph.getAllLinks()) {
                if ((link.firstNode == current && link.secondNode == array[current][1]) ||
                        (link.firstNode == array[current][1] && link.secondNode == current)) {

                    path.add(link);
                }
            }

            current = array[current][1];
        }

        if (current == -1) { //if we overflow to -1, then there's no path from end to start
            path.clear();
        }

        Collections.sort(path);

        if (end < start) {
            Collections.reverse(path);
            path.forEach(link -> {
                int holder = link.secondNode;
                link.secondNode = link.firstNode;
                link.firstNode = holder;
            });
        }

        return path;
    }

}
