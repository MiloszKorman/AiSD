package graph.services;

import graph.Graph;
import graph.Graph.Link;

import java.util.List;

public class SimpleConsoleVisualizationService implements GraphVisualizationService {

    @Override
    public void visualize(Graph graph) {
        for (int i = 0; i < graph.size(); i++) {
            System.out.print(i + 1);
        }
        System.out.println();
        System.out.println();

        for (Link link : graph.getAllLinks()) {
            System.out.print(link.firstNode + 1);
            for (int i = 0; i < link.weight; i++) {
                System.out.print("-");
            }
            System.out.print(link.secondNode + 1);

            System.out.println();
        }
    }

    @Override
    public void visualizeWithShortestPath(Graph graph, List<Link> path) {
        visualize(graph);
        System.out.println();
        System.out.println();
        System.out.println("Shortest path:");
        for (Link link : path) {
            System.out.print("\\u001B[31m" + link.firstNode + " ");
            System.out.print("\\u001B[31m" + link.secondNode + " ");
            System.out.println("\\u001B[0m" + link.weight);
        }
    }

}
