package graph.services;

import graph.Graph;
import graph.Graph.Link;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class SwingGraphVisualizationService implements GraphVisualizationService {

    private Graph graph;

    private List<Link> path = null;

    private int size;

    @Override
    public void visualize(Graph graph) {
        this.graph = graph;

        size = (graph.size()/2)*50;

        if (graph.size() != 0) {
            JFrame mainFrame = new JFrame();
            mainFrame.setTitle("Visual Graph Representation");
            mainFrame.setSize(size, size+50);
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            JPanel mainPanel = new Painting();
            mainPanel.setPreferredSize(new Dimension(size+50, size+50));
            mainFrame.add(mainPanel);
            mainFrame.setVisible(true);
        }
    }

    @Override
    public void visualizeWithShortestPath(Graph graph, List<Link> path) {
        this.graph = graph;
        this.path = path;

        size = (graph.size()/2)*100;

        if (graph.size() != 0) {
            JFrame mainFrame = new JFrame();
            mainFrame.setTitle("Visual Graph Representation");
            mainFrame.setSize(size, size+50);
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            JPanel mainPanel = new Painting();
            mainPanel.setPreferredSize(new Dimension(size+50, size+50));
            mainFrame.add(mainPanel);
            mainFrame.setVisible(true);
        }
    }

    class Painting extends JPanel {

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            g.setColor(Color.BLACK);

            double angle = 2 * Math.PI / graph.size(); //set angle of spacing between elements

            for (int elem = 0; elem < graph.size(); elem++) { //draw each of the elements
                int middleX = (int) ((size/2) + Math.sin(angle*elem) * (size/2-50));
                int middleY = (int) ((size/2) + Math.cos(angle*elem) * (size/2-50));
                g.fillOval(middleX-25, size - middleY - 25, 50, 50);
            }

            Graphics2D g2 = (Graphics2D) g;

            for (Link link : graph.getAllLinks()) { //draw links between elements (including knots) with their weight as thickness

                g2.setStroke(new BasicStroke(link.weight));

                if (link.firstNode == link.secondNode) {
                    g2.drawOval(
                            (int) ((size/2) + Math.sin(angle*(link.firstNode)) * (size/2-25)) - 25,
                            size -(int) ((size/2) + Math.cos(angle*(link.firstNode)) * (size/2-25)) - 25,
                            50,
                            50
                    );
                } else {
                    g2.drawLine(
                            (int) ((size/2) + Math.sin(angle*(link.firstNode)) * (size/2-50)),
                            size -(int) ((size/2) + Math.cos(angle*(link.firstNode)) * (size/2-50)),
                            (int) ((size/2) + Math.sin(angle*(link.secondNode)) * (size/2-50)),
                            size -(int) ((size/2) + Math.cos(angle*(link.secondNode)) * (size/2-50))
                    );
                }

            }

            g.setColor(Color.RED);
            if (path != null) {
                for (Link link : path) { //draw links for shortest path between elements in red, if such has been provided
                    g2.setStroke(new BasicStroke(link.weight));

                    if (link.firstNode == link.secondNode) {
                        g2.drawOval(
                                (int) ((size/2) + Math.sin(angle*(link.firstNode)) * (size/2-25)) - 25,
                                size -(int) ((size/2) + Math.cos(angle*(link.firstNode)) * (size/2-25)) - 25,
                                50,
                                50
                        );
                    } else {
                        g2.drawLine(
                                (int) ((size/2) + Math.sin(angle*(link.firstNode)) * (size/2-50)),
                                size -(int) ((size/2) + Math.cos(angle*(link.firstNode)) * (size/2-50)),
                                (int) ((size/2) + Math.sin(angle*(link.secondNode)) * (size/2-50)),
                                size -(int) ((size/2) + Math.cos(angle*(link.secondNode)) * (size/2-50))
                        );
                    }
                }
            }

            g.setColor(Color.WHITE);

            for (int elem = 0; elem < graph.size(); elem++) { //enumerate elements
                int middleX = (int) ((size/2) + Math.sin(angle*elem) * (size/2-50));
                int middleY = (int) ((size/2) + Math.cos(angle*elem) * (size/2-50));
                g.drawString(""+elem, middleX, size - middleY);
            }

        }

    }

}
