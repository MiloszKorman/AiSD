package graph;

import graph.Graph.Link;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GraphTest {

    public static void graphTest(Graph graph) {
        createGraph(graph);
        checkGraph(graph);

        graph.deleteElement(0);
        List<Link> links = graph.getAllLinks();

        assertEquals(0, links.get(0).firstNode);
        assertEquals(0, links.get(0).secondNode);
        assertEquals(1, links.get(0).weight);

        assertEquals(1, links.get(1).firstNode);
        assertEquals(3, links.get(1).secondNode);
        assertEquals(5, links.get(1).weight);

        assertEquals(2, links.get(2).firstNode);
        assertEquals(3, links.get(2).secondNode);
        assertEquals(3, links.get(2).weight);

        graph.removeLink(1, 3);
        links = graph.getAllLinks();

        assertEquals(2, links.size());

        assertEquals(0, links.get(0).firstNode);
        assertEquals(0, links.get(0).secondNode);
        assertEquals(1, links.get(0).weight);

        assertEquals(2, links.get(1).firstNode);
        assertEquals(3, links.get(1).secondNode);
        assertEquals(3, links.get(1).weight);

        graph.changeLinksWeight(0, 0, 10);
        links = graph.getAllLinks();

        assertEquals(0, links.get(0).firstNode);
        assertEquals(0, links.get(0).secondNode);
        assertEquals(10, links.get(0).weight);

        assertEquals(2, links.get(1).firstNode);
        assertEquals(3, links.get(1).secondNode);
        assertEquals(3, links.get(1).weight);
    }

    public static void createGraph(Graph graph) {
        graph.addLink(0, 0);
        graph.addLink(1, 1);
        graph.addNewElement();
        graph.addNewElement();
        graph.addNewElement();
        graph.addLink(2, 4, 5);
        graph.addLink(3, 4, 3);
    }

    public static void checkGraph(Graph graph) {
        List<Link> links = graph.getAllLinks();

        assertEquals(0, links.get(0).firstNode);
        assertEquals(0, links.get(0).secondNode);
        assertEquals(1, links.get(0).weight);

        assertEquals(1, links.get(1).firstNode);
        assertEquals(1, links.get(1).secondNode);
        assertEquals(1, links.get(1).weight);

        assertEquals(2, links.get(2).firstNode);
        assertEquals(4, links.get(2).secondNode);
        assertEquals(5, links.get(2).weight);

        assertEquals(3, links.get(3).firstNode);
        assertEquals(4, links.get(3).secondNode);
        assertEquals(3, links.get(3).weight);
    }

}