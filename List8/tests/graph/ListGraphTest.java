package graph;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ListGraphTest {

    @Test
    public void listGraphTest() {
        GraphTest.graphTest(new ListGraph());
        assertThrows(IllegalArgumentException.class, () -> new ListGraph(1));
    }

}