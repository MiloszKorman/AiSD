package graph;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class MatrixGraphTest {

    @Test
    public void matrixGraphTest() {
        GraphTest.graphTest(new MatrixGraph());
        assertThrows(IllegalArgumentException.class, () -> new MatrixGraph(1));
    }

}