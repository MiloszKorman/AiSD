package graph.services;

import graph.Graph;
import graph.GraphTest;
import graph.ListGraph;
import graph.MatrixGraph;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GraphStorageServiceTest {

    public static void savingTest(GraphStorageService storageService) {
        Graph graph = new MatrixGraph();
        GraphTest.createGraph(graph);
        GraphTest.checkGraph(graph);

        storageService.exportGraph("graphSave", graph);
        graph = storageService.importGraph("graphSave");

        GraphTest.checkGraph(graph);

        graph = new ListGraph();

        storageService.exportGraph("graphSave", graph);
        graph = storageService.importGraph("graphSave");

        assertEquals(0, graph.getAllLinks().size());
        assertEquals(2, graph.size());
    }

}