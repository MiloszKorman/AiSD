package graph.services;

import graph.Graph;
import graph.ListGraph;
import graph.MatrixGraph;
import graph.Graph.Link;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ShortestPathDijkistraFinderTest {

    @Test
    void findShortestPathTest() {

        Graph graph = new MatrixGraph(6);

        graph.addLink(0, 4, 3);
        graph.addLink(0, 5, 6);
        graph.addLink(4, 5, 2);
        graph.addLink(0, 1, 3);
        graph.addLink(1, 2, 1);
        graph.addLink(2, 5, 1);
        graph.addLink(1, 3, 3);
        graph.addLink(2, 3, 3);
        graph.addLink(5, 3, 1);

        List results = Arrays.asList(
                new Link(0, 1, 3),
                new Link(1, 3, 3)
        );

        assertEquals(results, ShortestPathDijkistraFinder.findShortestPath(graph, 0, 3));


        graph = new MatrixGraph(4);

        graph.addLink(0, 0, 1);
        graph.addLink(2, 2, 1);
        graph.addLink(0, 2, 4);
        graph.addLink(0, 1, 2);
        graph.addLink(2, 3, 5);
        graph.addLink(1, 3, 20);

        results = Arrays.asList(
                new Link(0, 2, 4),
                new Link(2, 3, 5)
        );

        assertEquals(results, ShortestPathDijkistraFinder.findShortestPath(graph, 0, 3));

        graph = new ListGraph(3);

        graph.addLink(1, 2, 3);

        assertEquals(0, ShortestPathDijkistraFinder.findShortestPath(graph, 0, 2).size());

        graph = new ListGraph(4);
        graph.addLink(1, 2);
        graph.addLink(2, 3);

        results = Arrays.asList(
                new Link(3, 2, 1),
                new Link(2, 1, 1)
        );

        assertEquals(results, ShortestPathDijkistraFinder.findShortestPath(graph, 3, 1));
    }

}