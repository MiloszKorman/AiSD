import graph.Graph;
import graph.MatrixGraph;
import services.PrimVisualizationFX;

public class Runner {

    public static void main(String[] args) {
        Graph graph = new MatrixGraph(4);
        graph.addEdge(0, 1, 2);
        graph.addEdge(1, 2, 2);
        graph.addEdge(0, 3);
        graph.addEdge(1, 3, 5);
        graph.addEdge(2, 3, 10);
        graph.addEdge(2, 2);
        PrimVisualizationFX.visualizePrim(graph);
    }

}
