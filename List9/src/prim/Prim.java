package prim;

import graph.Graph;
import graph.Graph.Link;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Prim {

    private final Graph graph;
    private List<Link> historyLinks;

    public Prim(Graph graph) {
        this.graph = graph;
        this.historyLinks = new LinkedList<>();
        calcPrim();
    }

    /**
     * Calculates minimum spanning tree
     * Takes list of 2-element links in the graph, sorted by links weight, so that we always choose cheapest path
     */
    private void calcPrim() {
        List<Integer> inTheTree = new ArrayList<>();
        List<Link> sortedList = graph.getAllLinks().stream()
                .filter(link -> link.firstNode != link.secondNode)
                .sorted(Comparator.comparingInt(link -> link.weight))
                .collect(Collectors.toList());

        while (!sortedList.isEmpty()) {
            Link link = sortedList.remove(0);
            if (!inTheTree.contains(link.firstNode) || !inTheTree.contains(link.secondNode)) {
                historyLinks.add(link);
            }
            if (!inTheTree.contains(link.firstNode)) {
                inTheTree.add(link.firstNode);
            }
            if (!inTheTree.contains(link.secondNode)) {
                inTheTree.add(link.secondNode);
            }
        }
        if (inTheTree.size() != graph.size()) {
            historyLinks.clear();
        }
    }

    public int getWeightsSum() {
        if (historyLinks.isEmpty()) {
            return -1;
        } else {
            return historyLinks.stream()
                    .map(link -> link.weight)
                    .reduce(0, (acc, linkWeight) -> acc + linkWeight);
        }
    }

    /**
     * @return links of minimum spanning tree
     */
    public List<Link> getLinks() {
        return historyLinks;
    }

}
