package services;

import graph.Graph;
import graph.Graph.Link;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import prim.Prim;

import java.util.List;

/**
 * Displays provided graph with emphasis on first element of minimum spanning tree, later each mouse clicked triggers
 * display of next minimum spanning tree link
 */
public class PrimVisualizationFX extends Application {

    private static Graph graph;
    private static List<Link> primeLines;

    @Override
    public void start(Stage primaryStage) throws Exception {

        Prim prim = new Prim(graph);
        int size = (graph.size() / 2) * 100;
        double angle = 2 * Math.PI / graph.size();

        Pane root = new Pane();
        primeLines = prim.getLinks();

        displayGraph(root, size, angle);

        root.setOnMouseClicked(e -> {
            displayNextPrimLine(size, root, angle);
        });

        Scene scene = new Scene(root, size, size);
        primaryStage.setTitle("Simple application");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void displayGraph(Pane root, int size, double angle) {

        for (int elem = 0; elem < graph.size(); elem++) {
            int middleX = (int) ((size / 2) + Math.sin(angle * elem) * (size / 2 - 50));
            int middleY = (int) ((size / 2) + Math.cos(angle * elem) * (size / 2 - 50));
            Circle circle = new Circle(middleX, middleY, 25);
            root.getChildren().add(circle);
        }

        for (Link link : graph.getAllLinks()) {
            if (link.firstNode == link.secondNode) {
                Circle circle = new Circle(
                        (int) ((size / 2) + Math.sin(angle * link.firstNode) * (size / 2 - 25)),
                        size - (int) ((size / 2) + Math.cos(angle * link.firstNode) * (size / 2 - 25)),
                        25
                );
                circle.setFill(Color.TRANSPARENT);
                circle.setStroke(Color.BLACK);
                root.getChildren().add(circle);
            } else {
                Line line = new Line(
                        (int) ((size / 2) + Math.sin(angle * (link.firstNode)) * (size / 2 - 50)),
                        size - (int) ((size / 2) + Math.cos(angle * (link.firstNode)) * (size / 2 - 50)),
                        (int) ((size / 2) + Math.sin(angle * (link.secondNode)) * (size / 2 - 50)),
                        size - (int) ((size / 2) + Math.cos(angle * (link.secondNode)) * (size / 2 - 50))
                );
                line.setStrokeWidth(link.weight);
                root.getChildren().add(line);
            }
        }

        if (!primeLines.isEmpty()) {
            int first = primeLines.get(0).firstNode;
            Circle circle = new Circle(
                    ((size / 2) + Math.sin(angle * first) * (size / 2 - 50)),
                    size - ((size / 2) + Math.cos(angle * first) * (size / 2 - 50)),
                    25
            );
            circle.setStroke(Color.RED);
            circle.setFill(Color.RED);
            root.getChildren().add(circle);
        }

        for (int elem = 0; elem < graph.size(); elem++) {
            Text text = new Text(
                    ((size / 2) + Math.sin(angle * elem) * (size / 2 - 50)),
                    size - ((size / 2) + Math.cos(angle * elem) * (size / 2 - 50)),
                    "" + elem
            );
            text.setStroke(Color.WHITE);
            root.getChildren().add(text);
        }

    }

    private void displayNextPrimLine(int size, Pane root, double angle) {
        if (!primeLines.isEmpty()) {
            Link link = primeLines.remove(0);
            Line line = new Line(
                    (int) ((size / 2) + Math.sin(angle * (link.firstNode)) * (size / 2 - 50)),
                    size - (int) ((size / 2) + Math.cos(angle * (link.firstNode)) * (size / 2 - 50)),
                    (int) ((size / 2) + Math.sin(angle * (link.secondNode)) * (size / 2 - 50)),
                    size - (int) ((size / 2) + Math.cos(angle * (link.secondNode)) * (size / 2 - 50))
            );
            line.setStroke(Color.RED);
            line.setStrokeWidth(link.weight);
            root.getChildren().add(line);
        }
    }

    public static void visualizePrim(Graph graph) {
        PrimVisualizationFX.graph = graph;
        launch(PrimVisualizationFX.class);
    }

}
