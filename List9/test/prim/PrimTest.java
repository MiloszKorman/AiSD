package prim;

import graph.Graph;
import graph.ListGraph;
import graph.MatrixGraph;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PrimTest {

    @Test
    public void testPrim() {

        //test correct links list when graph with minimum spanning tree is passed
        Graph graph = new MatrixGraph(4);
        graph.addEdge(0, 1, 2);
        graph.addEdge(1, 2, 2);
        graph.addEdge(0, 3);
        graph.addEdge(1, 3, 5);
        graph.addEdge(2, 3, 10);
        Prim prim = new Prim(graph);
        assertEquals(5, prim.getWeightsSum());
        assertEquals(Arrays.asList(
                new Graph.Link(0, 3, 1),
                new Graph.Link(0, 1, 2),
                new Graph.Link(1, 2, 2)
        ), prim.getLinks());

        //test empty list returned, when graph without minimum spanning tree is passed
        graph = new ListGraph(3);
        graph.addEdge(0, 1);
        prim = new Prim(graph);
        assertEquals(-1, prim.getWeightsSum());
        assertTrue(prim.getLinks().isEmpty());
    }

}